![](Logo_blue-bckgrd.png)
#       Superflat_Scripts

## Description
**A set of Python scripts to standardize metrology data processing among the participant to the Superflat PCP project**


## Last updates

### _31/05/2023:_
New features added since November 2022:

-	Computation of the PSD when the ROI contains a small number of invalid data points.

	After applying the Tukey window, Invalid data points are interpolated
	by cubic splines. If the number of bad data points in a line exceeds 20%
	this line is discarded from further computation.
	The percentage of invalid points in the ROI is printed to the console.

-	Computation of the sagittal slope errors.
-	Detrending by an X and Y polynomial of specified degree.

-	Saving the processing results in a pickle file for later editing. 
-	Bug fixes: 
	- Extraction of pixel size from some MX files.
	- Extra blank lines in .xyz files.

### _15/11/2022:_
ROI selection window now displays rectangle coordinates when dragging with the mouse. 2<sup>nd</sup> degree detrending was fixed to handle 'invalid data'.

Separate class for .datx file handling. Should work for nearly all MetroPro or MX version.
Some older datx versions used szip compression. They are readable providing more recenf Hdf5 dlls than the standard ones are installed in the h5py directory. 

## Installation
Superflat_Scripts package requires the following scripts are installed:
- h5py
- numpy 
- scipy
- matplotlib

It also uses the following packages which should be available with a basic **Conda** installation
- math
- tkinter
- pathlib

####   Installation commands with Conda
   Optionally create a new environment
   
      conda create -n superflat numpy scipy h5py matplotlib
   
   or install the required packages in an existing environment  
   
      conda activate myenv
      conda install numpy scipy h5py matplotlib

   Download the Superflat scripts package, either with git command
   
      git clone https://gitlab.synchrotron-soleil.fr/OPTIQUE/leaps/superflat_scripts

   either by getting and unpacking the zip file available from the Superflat git repository
      
      https://gitlab.synchrotron-soleil.fr/OPTIQUE/leaps/superflat_scripts/-/archive/main/superflat_scripts-main.zip
      
   To update the package with a newer version, enter the following command from your superflat_scripts directory
   
      git pull https://gitlab.synchrotron-soleil.fr/OPTIQUE/leaps/superflat_scripts

## Usage

Typical use is given by Superflat_main.py. Comment included in this file should help to taylor a script matching your needs

After activating your environment and switching to Superflat_scripts directory simply type:

      python Superflat_main.py

## Support
In case you are missing some functionnality or have suggestions for upgrades, please contact Fançois Polack: Francois.polack@synchrotron-soleil.fr

## known issues
We observed some internal variability in _.datx_ file of different origins. 
The present implementation is working with _.datx_ files where Surface is stored contiguously without compression.
Reading will fail if the Surface dataset is stored in a chunked compressed mode. Please report, with the console output, if you face such an issue.

Some _.datx_ files seem to contain more than one surface dataset. **HeightMap.read_Zygofile** will read the first "Surface" dataset pointed in the **/MetaData** table

## Contributing

This package was started on sample scripts provided by Uwe Flechsig <uwe.flechsig@psi.ch>. It has been extended by Fançois Polack <francois.polack@synchrotron-soleil.fr>,
 namely to include input from main Zygo  interferomtric data formats, exclude invalid data from computations and a prescribed percentage of outlier pixels from the statistics


## Authors and acknowledgment
- Fançois POLACK <francois.polack@synchrotron-soleil.fr>
- Uwe FLECHSIG <uwe.flechsig@psi.ch>
- Raymond BARRETT <barrett@esrf.fr>t

## License
This is an open source projects, no idea of the kind of license we should claim for

## Project status
Version 1.0  First release for test pupose

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)


