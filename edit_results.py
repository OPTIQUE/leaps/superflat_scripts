# -*- coding: utf-8 -*-

'''
This file is part of the Superflat Evaluation Toolkit

Main Superflat Evaluation routine

'''

__author__  = "François Polack, Uwe Flechsig"
__copyright__ = "2022, Superflat-PCP project"
__email__ = "superflat-pcp@synchrotron-soleil.fr"
__version__ = "0.1.0"

import sys,warnings
sys.path.append('D:\projets\ProjetsPython\Superflat')
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.figure as fig
import heightmap
from pathlib import Path
from scipy import ndimage as img


def getLabel(hmap):
    (label,color,lw,ls)=hmap.get_curveparms()                                        
    try :
        label+=' - '+hmap.detrend
    except AttributeError:
        pass
    except TypeError:
        pass
    return label
def plot_slope_rms(maplist, ymax=1., comment='' ) :
    """! Display a graph of the cumulative rms slope error versus spatial frequency
        @param comment an optional comment which will be appended to the graph title

        The cumulative rms slope error is the square root of the slope CPSD
    """
    graph=plt.figure(figsize=fig.figaspect(0.5))  # new plot window
    for hmap in maplist :
        (label,color,linewidth, linestyle)=hmap.get_curveparms()                                                           
        plt.plot(hmap.sf*1e-3, hmap.srms*1e6, label=label,color=color, lw=linewidth, ls=linestyle)
    plt.grid(visible=True, which='major', axis='both')
    # plt.axis([.01,3., 0, ymax])
    # plt.semilogx()
    plt.loglog()
    # graph.axes[0].set_xlim([0.01,10])
    plt.xlabel(r'spatial frequency $(mm^{-1})$')
    plt.ylabel('rms slope (µrad)')
    plt.title('Cumulated RMS tangential slopes {}'.format(comment))
    plt.legend()


def plot_height_rms(maplist, ymax=0.5, comment='' ) :
    """! Display a graph of the cumulative rms height error versus spatial frequency
        @param comment an optional comment which will be appended to the graph title

        The cumulative rms Height error is the square root of the Height CPSD
    """
    graph=plt.figure(figsize=fig.figaspect(0.5))  # new plot window
    for hmap in maplist :
        if type(hmap.hrms) == type(None):
            return
        (label,color,linewidth, linestyle)=hmap.get_curveparms()  
        plt.plot(hmap.sf*1e-3, (hmap.hrms[-1]-hmap.hrms)*1e9, label=label,color=color, lw=linewidth, ls=linestyle)
    plt.grid(visible=True, which='major', axis='both')
    # plt.axis([.01,10., 0, ymax])
    plt.semilogx()
    # plt.loglog()    
    graph.axes[0].set_ylim([0,ymax])
    plt.xlabel(r'spatial frequency $(mm^{-1})$')
    plt.ylabel('rms height (nm)')

    plt.title('Cumulated RMS tangential heights {}'.format(comment))
    plt.legend()



def slope_map(maplist,direction='x', filter=None, vbounds=None, color='rainbow'):
    ''' Display all slop maps in black and white with identical scale
        @param direction the derivation direction 'x'  or  'y'
        @param filter The cutoff frequency (mm^-1) of the gaussian filter used for display
        @param vbounds = [vmax,Vmin] is the display range in nrad
        @param color is a code for cmap https://matplotlib.org/stable/gallery/color/colormap_reference.html
    '''

                                                                                                                                                                     
    for hmap in maplist :
        warnings.simplefilter('ignore', category=UserWarning)
        slopemap=plt.figure(figsize=fig.figaspect(hmap.dzdx), constrained_layout=True)
        warnings.resetwarnings()
                                                      
                                                                                                                                                                            

        # slopemap.gca().invert_yaxis()  # images are scanned in line order, from top left to bottom right
        
        
        if direction=='x':
            if filter==None:
                sdata=hmap.dzdx
            else:
                sigma=3e-4/filter/hmap.pixel[0]
                sdata=img.gaussian_filter1d(hmap.dzdx, sigma, axis=1)
            title=getLabel(hmap)+' Tangential Slopes'
        elif direction=='y':
            if filter==None:
                sdata=hmap.dzdy
            else:
                sigma=3e-4/filter/hmap.pixel[0]
                sdata=img.gaussian_filter1d(hmap.dzdy, sigma, axis=0)
            title=getLabel(hmap)+' Sagittal Slopes'
        else:
            print("Undefined direction:", direction)
            return


        if vbounds==None:
            bounds=np.array([0.1, 99.9])
            #we may have masked values percentile do no work with masked arrays we need to force it
            vmask=~sdata.mask
            vbounds=np.percentile(sdata[vmask],bounds)*1e6
        

        #plt.pcolormesh(self.x * 1e3, self.y * 1e3, self.dzdx * 1e6, cmap=plt.get_cmap('rainbow'),
        #               vmin=np.min(hmap.dzdx) * 1e6, vmax=np.max(self.dzdx) * 1e6, shading='auto')

        plt.pcolormesh(hmap.x*1e3, hmap.y*1e3, sdata*1e6, cmap=plt.get_cmap(color),
                            vmin=vbounds[0], vmax=vbounds[1], shading='auto')

        cbar = plt.colorbar(aspect=50)
        zlabel = 'X slope (µrad)'
        cbar.set_label(zlabel) #, rotation=270)
        plt.xlabel('x (mm)')
        plt.ylabel('y (mm)')
        plt.title(title)
        
def herr_map(maplist, vbounds=None, color='rainbow'):
    for hmap in maplist :
        warnings.simplefilter('ignore', category=UserWarning)
        errmap=plt.figure(figsize=fig.figaspect(hmap.ze), constrained_layout=True)
        warnings.resetwarnings()

        # errmap.gca().invert_yaxis()  # images are scanned in line order, from top left to bottom right
        

        if vbounds==None:
            bounds=np.array([0.1, 99.9])
            #we may have masked values percentile do no work with masked arrays we need to force it
            vmask=~hmap.ze.mask
            vbounds=np.percentile(hmap.ze[vmask],bounds)*1e9
        

        #plt.pcolormesh(self.x * 1e3, self.y * 1e3, self.dzdx * 1e6, cmap=plt.get_cmap('rainbow'),
        #               vmin=np.min(hmap.dzdx) * 1e6, vmax=np.max(self.dzdx) * 1e6, shading='auto')

        plt.pcolormesh(hmap.x*1e3, hmap.y*1e3, hmap.ze*1e9, cmap=plt.get_cmap(color),
                            vmin=vbounds[0], vmax=vbounds[1], shading='auto')

        cbar = plt.colorbar(aspect=50)
        zlabel = 'X slope (µrad)'
        cbar.set_label(zlabel) #, rotation=270)
        plt.xlabel('x (mm)')
        plt.ylabel('y (mm)')
        plt.title(getLabel(hmap)+" Height differences")

