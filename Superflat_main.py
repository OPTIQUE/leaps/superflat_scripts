# -*- coding: utf-8 -*-

'''
This file is part of the Superflat Evaluation Toolkit

Main Superflat Evaluation routine

'''

__author__  = "François Polack, Uwe Flechsig"
__copyright__ = "2022, Superflat-PCP project"
__email__ = "superflat-pcp@synchrotron-soleil.fr"
__version__ = "0.1.0"


import numpy as np
import matplotlib.pyplot as plt
import heightmap

#print( "toolbar default param:",plt.rcParams['toolbar'] )


plt.close('all')           # close old plot windows
hmp = heightmap.HeightMap()          # create a new HeightMap object


#loads a data file in the HeightMap
# (the ROI is set to fullsize)
if hmp.read_Zygofile()==False:
    print("file loading failure")
    quit()

# the loaded data may have invalid data, which will be masked out
# for display, statistics and fit computations
# Slope computations spread the invalid data and PSD evaluation will fail

# Plot the heightMap on the whole raw data definition area
hmp.plot2dheight(raw=True)

# if hmp.set_ROI(origin,size) is called here calling plot_select_ROI will display it for confirmation
#hmp.set_ROI(24, 9, 596, 70)

#  Interactive plot of 2nd order detrend data on whole defined area
#   Allowing to define a ROI rectangle for slope computation and statistics
#  display figure manager will block until this figure is closed by the 'validate ROI' or 'close' buttons
hmp.plot_select_ROI()
# the ROI is input when the select ROI button is clicked otherwise, it is left unchanged


print ("\nROI defined as :", hmp.ROIorigin, hmp.ROIsize)

#plot the absolute height on the selected ROI
hmp.plot2dheight(raw=True)

# 2d fit to finf best toiroid orientation (
fit=hmp.find_best_toroid()

print("\nToroid detrending on ROI")
fit=hmp.find_best_toroid()
#the best toroid axis angle is returned in the first fit parameter
alpha=fit[0]
# remove the best toroid with a given axis angle
# we should probably limit this angle below 20 mrad
hmp.remove_toroid(rot=alpha)


# Plot the detended heights on ROI The display scale is set for 99.8% of the pixel to fit in
hmp.plot2dheight(contour=False, percent=0.2)

# Print detrended height statistics  on ROI
# The display statistics are computed on the 99% pixels closest to the mean value
hmp.print_statistics(height=True, percent=1)

# compute the X derivative by central difference method
hmp.compute_x_slope()

# plot the slope errors with a display scale fitting 99.8% of the pixels
hmp.plot2dslope(percent=0.2)
#print the slope statistic limited to the 'best 99% pixels'
hmp.print_statistics(height=False, percent=1)

# Compute the heiht PSD and  from it, the slope psd and CPSD
# The ROI must not contain invalid data. A warling will be issue if not, but no fatal error will result
hmp.compute_CPSD()
# plot the cumulative rms slope error,  versus spatial frequency. This is the square root of slope CPSD
hmp.plot_slope_rms()

# Blocking call to the figure display manager, allowing to save them eventually
# Will return when all figures are closed
plt.show()  # show figures until they are all closed

# end

