# -*- coding: utf-8 -*-

'''
This file is part of the Superflat Evaluation Toolkit

Evaluation of surface height data based on  PSD tools
This file defines the ActiveFigure class, which is the main storage element of surface height measurements

'''

__author__  = "François Polack"
__copyright__ = "2022, Superflat-PCP project"
__email__ = "superflat-pcp@synchrotron-soleil.fr"
__version__ = "0.1.0"

import numpy as np
import warnings
import matplotlib.pyplot as plt
import matplotlib.figure as fig
import matplotlib.patches as patches
from matplotlib.backend_tools import ToolBase   #, ToolCursorPosition   #, ToolToggleBase
from processing import  fit2D_order2, fit2D_poly


def on_press(event):
    """ Activated by a mouse button down event and relayed to the relevant figure"""
    # print("figure", event.canvas.figure.number, event.button, event.xdata, event.ydata)
    if isinstance(event.canvas.figure, ActiveFigure):
        event.canvas.figure.on_press(event)
    else:
        print( " Not ActiveFigure class")

def on_mouse_move(event):
    """ Activated by a mouse move event and relayed to the relevant figure"""
    if isinstance(event.canvas.figure, ActiveFigure):
        event.canvas.figure.on_mouse_move(event)
    elif isinstance(event.canvas.figure, Viewer2D):
        event.canvas.figure.on_mouse_move(event)

def on_mouse_drag(event):
    """ Activated by a mouse move event and relayed to the relevant figure"""
    if isinstance(event.canvas.figure, ActiveFigure):
        event.canvas.figure.on_mouse_drag(event)

def on_release(event):
    """ Activated by a mouse button up event and relayed to the relevant figure"""
    # print("figure", event.canvas.figure.number, event.button, event.xdata, event.ydata)
    if isinstance(event.canvas.figure, ActiveFigure):
        event.canvas.figure.on_release(event)

def on_close(event):
    """ Activated by the wondow closing event of the figure and used to escape from interactive mode """
    #print ("closing figure", event.canvas.figure.number)
    event.canvas.stop_event_loop()

def find_pixel(ar, value):
    """ Find the nearest pixel index  for a location value in the x or y array  """
    size=ar.shape[0]
    # find the "insertion point" of the value in the X or Y array
    if ar[0] < ar[size-1]:
        index=ar.searchsorted(value)
    else:
        index=size-ar.searchsorted(value, sorter=np.arange(size-1,-1,-1))
   # we know that value is in range ar[index-1] ar[index] find which is the closest
   # special case if index= 0 or size
    if index==0:
        if abs((value-ar[0])/(ar[1]-ar[0]))< 0.5:
            return 0
        else:
            return -1
    elif index==size:
        if abs((value-ar[size-1])/(ar[size-1]-ar[size-2]))< 0.5:
            return size-1
        else:
            return -1
    else:
        if abs((value-ar[index])/(ar[index]-ar[index-1]))< 0.5:
            return index
        else:
            return index-1
   
            
class ROItool(ToolBase):
    """ Tool button used to validate the ROI rectangle defined in the host figure

       The trigger associated function records the ROI size and position and close the figure to unblock eventual processiing
    """
    # keyboard shortcut
    default_keymap = ('M','m')
    description = 'Record the marked area as ROI'
    def trigger(self, *args, **kwargs):
        """ Activated by clicking the `ROItool` button\n
            Validate the currently marked ROI
        """
        self.figure.hmap.set_ROI(self.figure.ROI[0],self.figure.ROI[1],self.figure.ROI[2],self.figure.ROI[3])
        # self.figure.hmap.ROIorigin=[self.figure.ROI[0],self.figure.ROI[1]]
        # self.figure.hmap.ROIsize=[self.figure.ROI[2],self.figure.ROI[3]]
        #print("saving ROI:",self.figure.ROI)
        print("ROI defined from figure")
        plt.close(self.figure)


class Viewer2D(fig.Figure):
    """" A Figure with ability of showing values at pointer position in the display area"""
    def __init__(self,*args, **kwargs):
        super().__init__(*args,**kwargs)
        self.selectaxes=None
        self.hmap=None
        self.origin=(0,0)
        self.z=None
        self.x=None
        self.y=None
        self.unit=''
        self.mvcid=0
        
    def on_mouse_move(self,event):
        smsg=""
        scale=1e9 if self.unit=='nm' or self.unit=='nrad' else 1e6 if self.unit=='µm' or self.unit=='µrad' \
                else 1e3 if  self.unit=='mm' or self.unit=='mrad' else 1
        if event.inaxes is  self.selectaxes :
            pos=(event.xdata,event.ydata)
            # px=int(round(1e-3*pos[0]/self.hmap.pixel[0],0)-self.origin[0])
            # py= int(round(1e-3*pos[1]/self.hmap.pixel[1],0)-self.origin[1])
            px=find_pixel(self.x, pos[0])
            py=find_pixel(self.y, pos[1])
            smsg="pos:({:d}, {:d})  [{:.1f}, {:.1f} ]mm, {:.3f} {:s}".format(px, py, pos[0],pos[1], scale*self.z[py,px],self.unit )
        event.canvas.manager.toolbar.set_message(smsg)
        
class ActiveFigure(fig.Figure):
    """" A Figure with a special button in the toolbar and  methods for drawing a rectangular ROI area"""
    def __init__(self,*args, **kwargs):
        super().__init__(*args,**kwargs)
        self.hmap=None
        """ The `HeightMap` object being edited """
        self.r1=None
        """ Region defining white rectangle """
        self.r2=None
        """ Black dashed rectangle superimposed to `ActiveFigure.r1` """
        self.pxorg=None

        self.ROI=[0,0,0,0]
        """ ROI rectangle set-up by the mouse up event """
        self.ROIwindow=[0,0,0,0]

        self.selectaxes=None
        """" Memorize the axes group where the mouse entered the drag mode """

        self.mvcid=0
        
        self.x=None
        self.y=None
        """ The arrays of pixel absolute positions. if they are set to none the old pointer location method is used """ 

    def on_press(self,event):
        """ Called after a mouse button down event.
            Initiate the drawing of a new rectangle
        """
        if self.r1 is not None:
            self.r1.set_visible(True)
            self.r2.set_visible(True)
            if event.inaxes is not None:
              #  self.selectaxes=event.inaxes
                org=(event.xdata,event.ydata)
                self.r1.set_xy(org)
                self.r2.set_xy(org)
                if type(self.x)==type(None):
                    self.ROI[0:2]=(int(round(1e-3*org[0]/self.hmap.pixel[0],0)-self.hmap.origin[0]),
                            int(round(1e-3*org[1]/self.hmap.pixel[1],0)-self.hmap.origin[1]) )
                else:
                    self.ROI[0:2]=(find_pixel(self.x, org[0]), find_pixel(self.y, org[1]))

                smsg="origin:({:d}, {:d}) size:({:d}, {:d})  [{:.1f}, {:.1f} : {:.1f}, {:.1f}]mm".format(
                self.ROI[0],self.ROI[1], 0, 0,  org[0],org[1],0, 0)
                event.canvas.manager.toolbar.set_message(smsg)

                event.canvas.mpl_disconnect(self.mvcid)
                self.mvcid = event.canvas.mpl_connect('motion_notify_event', on_mouse_drag)

    def on_mouse_drag(self,event):
        """ Called after a mouse move event. Update the rectangle drawing """
        if event.inaxes is self.selectaxes :
            org=self.r1.get_xy()
            width = event.xdata-org[0]
            height= event.ydata-org[1]
            self.r1.set_width(width)
            self.r1.set_height(height)
            self.r2.set_width(width)
            self.r2.set_height(height)

            if type(self.x)==type(None):
                self.ROI[2]=int(round(1e-3*width/self.hmap.pixel[0],0))
                self.ROI[3]=int(round(1e-3*height/self.hmap.pixel[1],0))
            else:
                self.ROI[2]=find_pixel(self.x, event.xdata)-self.ROI[0]+1
                self.ROI[3]=find_pixel(self.y, event.ydata)-self.ROI[1]+1

            smsg="origin:({:d}, {:d}) size:({:d}, {:d})  [{:.1f}, {:.1f} : {:.1f}, {:.1f}]mm".format(
            self.ROI[0],self.ROI[1], self.ROI[2], self.ROI[3],  org[0],org[1],width, height)
            event.canvas.manager.toolbar.set_message(smsg)

            event.canvas.draw()

    def on_mouse_move(self,event):
        smsg=""
        if event.inaxes is  self.selectaxes :
            pos=(event.xdata,event.ydata)
            if type(self.x)==type(None):
                px=int(round(1e-3*pos[0]/self.hmap.pixel[0],0)-self.hmap.origin[0])
                py= int(round(1e-3*pos[1]/self.hmap.pixel[1],0)-self.hmap.origin[1])
            else:
                px=find_pixel(self.x, pos[0])
                py=find_pixel(self.y, pos[1])
            if self.hmap.rawZ.mask[py,px]:
                smsg="pos:({:d}, {:d})  [{:.1f}, {:.1f} ]mm, --- µm".format(px, py, pos[0],pos[1])
            else:
                smsg="pos:({:d}, {:d})  [{:.1f}, {:.1f} ]mm, {:.3f} µm".format(px, py, pos[0],pos[1], self.hmap.rawZ[py,px]*1e6 )
        else:
            smsg="origin:({:d}, {:d}) size:({:d}, {:d})  [{:.1f}, {:.1f} : {:.1f}, {:.1f}]mm".format(
                self.ROI[0],self.ROI[1],self.ROI[2],self.ROI[3],self.ROIwindow[0],self.ROIwindow[1],self.ROIwindow[2],self.ROIwindow[3])
           # event.canvas.manager.toolbar.set_message(smsg)
        event.canvas.manager.toolbar.set_message(smsg)



    def on_release(self, event):
        """ Called after the mouse button is released.
            Update the  `ActiveFigure.ROI`variable to memorize the drawn rectangle, and print its extent on standard output
        """
        event.canvas.mpl_disconnect(self.mvcid)
        self.mvcid = event.canvas.mpl_connect('motion_notify_event', on_mouse_move)
        
        if type(self.x)==type(None):
            x0,y0=self.r1.get_xy()
            width=self.r1.get_width()
            if width < 0:
                width=-width
                x0-=width
            height=self.r1.get_height()
            if height < 0:
                height=-height
                y0-=height
            px0=int(round(1e-3*x0/self.hmap.pixel[0],0)-self.hmap.origin[0])
            pwidth=int(round(1e-3*width/self.hmap.pixel[0],0))
            py0=int(round(1e-3*y0/self.hmap.pixel[1],0)-self.hmap.origin[1])
            pheight=int(round(1e-3*height/self.hmap.pixel[1],0))
            self.ROI=[px0, py0, pwidth, pheight]
            self.ROIwindow=[x0, y0, width, height]
        else:
            if event.inaxes is self.selectaxes :
                xend=find_pixel(self.x, event.xdata)
                yend=find_pixel(self.y, event.ydata)
                self.ROI[2]=abs(xend-self.ROI[0])+1
                self.ROI[3]=abs(yend-self.ROI[1])+1
                if xend < self.ROI[0]:
                    self.ROI[0]=xend
                    self.ROIwindow[0]=self.x[xend]
                if yend < self.ROI[1]:
                    self.ROI[1]=yend
                    self.ROIwindow[1]=self.y[yend]
                w=self.ROI[2]*self.hmap.pixel[0] if self.x[0] < self.x[-1] else -self.ROI[2]*self.hmap.pixel[0]
                h=self.y[self.ROI[3]]*self.hmap.pixel[1] if self.y[0] < self.y[-1] else -self.y[self.ROI[3]]*self.hmap.pixel[1]
                self.ROIwindow=(self.x[self.ROI[0]],self.y[self.ROI[1]],w,h)
                                

        print("ROI window  (({:.3f}, {:.3f}), {:.3f}, {:.3f}) mm".
                      format(self.ROIwindow[0],self.ROIwindow[1],self.ROIwindow[2],self.ROIwindow[3]) )
        print("ROI position & shape: (", self.ROI[0], ",", self.ROI[1],")",  self.ROI[2], ",", self.ROI[3])


def new_active_figure(hmap, raw=False, percent=5, fit_degree=2):
    """ Create a new `ActiveFigure` for selecting the ROI of further computation

        Parameters
        ---------------\n
        `hmap' The HeightMap instance from where the figure will be plotted \n
        `raw (bool) if True: Plot the raw height map (`hmap.rawZ`) else plot a detrended heightmap with a 2D fitting polynomial of degree 2 \n
        `percent` define the figure plotting range, so that only `percent` of the total number of pixel have values outside this range
    """
    warnings.simplefilter('ignore', category=UserWarning)  # or 'once'

    # We plot a fullscale image and use fullscale for x and Y
    #   org and size no longer used here
    # size=(hmap.pixel[0]*hmap.rawZ.shape[1],hmap.pixel[1]*hmap.rawZ.shape[0])
    # org=(hmap.pixel[0]*hmap.origin[0], hmap.pixel[1]*hmap.origin[1])

    #  hmap. x,y rawX and rawY are now always defined
    # x=np.linspace(org[0],org[0]+size[0],num=hmap.rawZ.shape[1], endpoint=False)
    # y=np.linspace(org[1],org[1]+size[1],num=hmap.rawZ.shape[0], endpoint=False)
    x=hmap.rawX
    y=hmap.rawY
    # if not raw we nee to use a full size image and subtract a 2nd degree trend for best view
    if raw:
        z=hmap.rawZ
    else:
        #  (fitparams, z) = fit2D_order2(x,y,hmap.rawZ) 
        center=((x[0]+x[-1])/2., (y[0]+y[-1])/2.)
        if fit_degree==2:
            ((Z0, pitch, roll, x2, xy, y2), z) = fit2D_order2(x-center[0],y-center[1],hmap.rawZ)
        elif fit_degree==3:
            powers=((0,0),(1,0),(0,1),(2,0),(1,1),(0,2),(3,0))
            ((Z0, pitch, roll, x2, xy, y2,x3), z) = fit2D_poly(x-center[0],y-center[1],hmap.rawZ, powers)
        elif fit_degree==4:
            powers=((0,0),(1,0),(0,1),(2,0),(1,1),(0,2),(3,0),(0,3),(4,0),(0,4))
            ((Z0, pitch, roll, x2, xy, y2,x3, y3,x4,y4), z) = fit2D_poly(x-center[0], y-center[1], hmap.rawZ, powers)
        print('tip tilt',Z0, pitch, roll)
        print('2nd order',x2, xy, y2)
        if fit_degree >2:
            print('3rd order', x3,y3)
        if fit_degree >3:
            print('4th order', x4, y4)
            
        """
        print("Raw fit coeffs: Z0 = {:.3e}m, pitch = {:.4e}rad,  roll = {:.4e}rad, x2=  {:.3e}m-1, xy=  {:.3e}m-1, y2=  {:.3e}m-1".
            format(Z0, pitch, roll, x2, xy, y2))
        """
    scale= 'µm' if raw  else  'nm'
    title= 'Height' if raw else 'Height error'

    if(scale=="nm"):
        zscale=1.e9
        zlabel="Z nm"
    elif(scale=="µm"):
        zscale=1e6
        zlabel="Z µm"
    elif(scale=="mm"):
        zscale=1e3
        zlabel="Z mm"
    else:
        zscale=1
        zlabel="Z m"

    uu=zscale*z    #

    bounds=np.array([percent/2, 100-percent/2])

    #  vbounds=np.percentile(uu,bounds)
    #percentile do no work with masked arrays we need to force it

    vmask=~np.ma.getmaskarray(uu)
    vbounds=np.percentile(uu[vmask],bounds)

    #print( "map range: min/max = ", np.min(uu), np.max(uu), " percentile :" ,vbounds )

    (w, h)=fig.figaspect(z)

    #activating special toolbar
    stdmgr=plt.rcParams['toolbar']
    plt.rcParams['toolbar'] = 'toolmanager'
    # The warning seems to have been corrected in last updated versions
    # print("activating a figure with a managed toolbar, generates a warning which can'tbe turned-off:")
    activefig=plt.figure(FigureClass=ActiveFigure, figsize=(w, h), constrained_layout=True)
    #switching back to standard pyplot toolbar
    plt.rcParams['toolbar'] = stdmgr

    # activefig.gca().invert_yaxis()

    #we do not crowd the figure with contour line
    # if contour :
        # plt.contour(hmap.x*1e3, hmap.y*1e3, uu, 15, linewidths= 0.5, colors= 'k')  # contour lines
    activefig.x=1e3*x
    activefig.y=1e3*y
    plt.pcolormesh(activefig.x,activefig.y, uu, cmap=plt.get_cmap('rainbow'),
    # plt.pcolormesh(x*1e3,y*1e3, uu, cmap=plt.get_cmap('rainbow'),
                vmin=vbounds[0], vmax=vbounds[1], shading='auto')
                 #  vmin=np.min(uu), vmax=np.max(uu), shading='auto')

    cbar= plt.colorbar(aspect=15)
    cbar.set_label(zlabel) #, rotation=270)
    plt.xlabel('x (mm)')
    plt.ylabel('y (mm)')
    plt.title(title)

    activefig.ROI=[hmap.ROIorigin[0],hmap.ROIorigin[1],hmap.ROIsize[0],hmap.ROIsize[1]]
    #print("construction ROI pix:", activefig.ROI)
    #construct the ROI rectangle
    #   new definition using x and y arrays
    # org=((x[0]+hmap.pixel[0]*hmap.ROIorigin[0])*1e3, (y[0]+hmap.pixel[1]*hmap.ROIorigin[1])*1e3)
    org= (1e3*x[hmap.ROIorigin[0]], 1e3*y[hmap.ROIorigin[1]])
    width=1e3*hmap.pixel[0]*(hmap.ROIsize[0]-1) if x[0] < x[-1] else -1e3*hmap.pixel[0]*(hmap.ROIsize[0]-1)
    height=1e3*hmap.pixel[1]*(hmap.ROIsize[1]-1) if y[0] < y[-1] else -1e3*hmap.pixel[1]*(hmap.ROIsize[1]-1)
    activefig.ROIwindow=[org[0], org[1], width,height]
    #print ("ROI mm:",org, width, height)
    activefig.r1=patches.Rectangle(org, width, height, linewidth=1.5, edgecolor='w', facecolor='none', visible=True)
    activefig.r2=patches.Rectangle(org, width, height, linewidth=1.5,linestyle=(0,(5,5)), edgecolor='k', facecolor='none', visible=True)
    plt.gca().add_patch(activefig.r1)
    plt.gca().add_patch(activefig.r2)
    plt.connect('button_press_event', on_press)
    plt.connect('button_release_event', on_release)
    plt.connect('close_event', on_close)
    activefig.selectaxes=plt.gca().axes


    activefig.canvas.manager.toolmanager.add_tool('Validate marked ROI', ROItool)
    activefig.canvas.manager.toolbar.add_tool('Validate marked ROI', 'Superflat', 0)
   # activefig.canvas.manager.toolmanager.add_tool('position', ToolCursorPosition)
   # activefig.canvas.manager.toolbar.add_tool('position', 'Superflat', -1)
   # activefig.canvas.manager.toolbar.set_message('my message')

    activefig.hmap=hmap
    activefig.mvcid = activefig.canvas.mpl_connect('motion_notify_event', on_mouse_move)
    warnings.resetwarnings()
    return activefig
