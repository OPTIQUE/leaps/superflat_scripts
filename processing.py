# -*- coding: utf-8 -*-

'''
This file is part of the Superflat Evaluation Toolkit

Data processing functions used by this toolkit

'''

__author__  = "François Polack, Uwe Flechsig"
__copyright__ = "2022, Superflat-PCP project"
__email__ = "superflat-pcp@synchrotron-soleil.fr"
__version__ = "0.1.0"


import numpy as np
import scipy.signal
import nfft
from math import sin, cos

def fit2D_poly(xin,yin,z, powers,rot=0):
    """! The fit2D_poly`function find the best fit of a surface by a 2D polynomial of prescibed shape, and returns its coeffcients in a tuplet
        @param xin: (numpy.array):
            a 1D array of cooordinates along the fast varying dimension of array z
        @param yin: (numpy.array):
            a 1D array of cooordinates along the slow varying dimension of array z
        @param z: (numpy.ma.array):
            a 2D numpy masked array  constaining the value of the surface to be fitted
        @param powers: (tuplet list)
            a list of tuplets (xpow, ypow) respectively specifying the  exponent in x and y  of the allowed polynomial terms
            @f$(x^xpow y^ypow)@f$
        @param rot: [optional]
            the rotation angle [in rd] of the coordinate axis on which the polynomal fit is computed
        @return The tuplet (**coeffs**, **residues**)  (list, numpy.ma.array)
            - **coeffs** is  the list of the coefficients of the fitting polynomial, matching the **powers** list in the same order.
            - **residues**  is a masked array of the fit residuals, with the same mask as the input **z** array.

        For allowing to fit a surface in the presence of missing or invalid data at some points of the definition grid,
        the function requires a to receive the surface values in a masked array, the mask of which is screening invalid or unwanted values
    """


    # grid coords
    x, y = np.meshgrid(xin, yin)
    # print("fit2D_poly called with rotation angle:", rot)
    if(rot !=0 ):
        R=np.array([[np.cos(rot), np.sin(rot)],[-np.sin(rot),np.cos(rot)]])
        V=np.array([x.ravel(),y.ravel()])
        #print(R.shape, V.shape)
        U=R@V
        x=np.reshape(U[0,:], x.shape)
        y=np.reshape(U[1,:], y.shape)
    print( "shape X", x.shape, "shape Y", y.shape, "shape Z" , z.shape)
    x=np.ma.array(x,mask=z.mask)
    y=np.ma.array(y,mask=z.mask)
    #vmask=~z.mask  Not valid if z has no invalid data
    vmask=~np.ma.getmaskarray(z)
    valid=z[vmask].size
    print("Mask shape", z.mask.shape ,"Vmask shape",vmask.shape, "unmasked shape",z[vmask].shape)

    print ("valid data " , valid, "  num coeffs ", len(powers))

    a = np.ma.zeros((valid, len(powers)))
    for index,(i,j) in enumerate(powers):       #enumerate powers as columns
        aux= x**i * y**j
        a[:,index]=aux[vmask]                   #fill columns
    #print(a.shape); print(a)
    sol= np.linalg.lstsq(a, z[vmask], rcond=None)
    coeffs=sol[0]
    # compute residual on all points
    residues=z.copy()
    for index,(i,j) in enumerate(powers):
        residues-=coeffs[index] * x**i * y**j

    return (coeffs, residues) # return  the coeffs of the solution and the residual matrix

def fit_toroid(x,y,z, rot=0):
    """!     Fit a a surface to a second degree 2D polynomial with the following terms only:
    @f$(1,    x,    y,    x^{2},  y^2)@f$ excluding the xy term.
    @param x: (`numpy.array`):
        a 1D array of cooordinates along the fast varying dimension of array z
    @param y: (`numpy.array`):
        a 1D array of cooordinates along the slow varying dimension of array z
    @param z: (`numpy.ma.array`):
        a 2D numpy masked array  constaining the value of the surface to be fitted
    @param rot: [optional]
        the rotation angle of the coordinate axis on which the polynomal fit is computed
    @return The tuplet (**coeffs**, **residues**) : (list, numpy.ma.array`)
        - **coeffs**: the list of the coefficients of the fitting polynomial, in the order :  ( 1,    x,    y,    x^2,  y^2 )
        - **residues**:  a masked array of the fit residuals, with the same mask as the input `z` array.

    The fonction calls processing.fit2D_poly
     """
    powers=((0,0),(1,0),(0,1),(2,0),(0,2))
    return fit2D_poly(x,y,z,powers,rot)

def fit2D_order2(x,y,z,rot=0):
    """!
    Fits a a surface to a 2D polynomial with all second order terms terms
    @f$( 1, x, y, x^2, xy, y^2)@f$
    @param x : (`numpy.array`):
        a 1D array of cooordinates along the fast varying dimension of array z
    @param y : (`numpy.array`):
        a 1D array of cooordinates along the slow varying dimension of array z
     @param z: (`numpy.ma.array`):
        a 2D numpy masked array  constaining the value of the surface to be fitted
    @param rot: [optional]
        the rotation angle (in rd] of the coordinate axis on which the polynomal fit is computed
    @return The tuplet (**coeffs**, **residues**) : (list, numpy.ma.array`)
        - **coeffs**: the list of the coefficients of the fitting polynomial, in the order @f$( 1, x, y, x^2, xy, y^2)@f$
        - **residues**:  a masked array of the fit residuals, with the same mask as the input z array.

    The function calls processing.fit2D_poly
    """

    powers=((0,0),(1,0),(0,1),(2,0),(1,1),(0,2))
    return fit2D_poly(x,y,z,powers,rot)

def fit_toroid1(xin,yin,z):
    """!
    @deprecated    Same as `processing.fit_toroid`, but without axis rotation and without calling `processing.fit2D_poly`
    """
    # grid coords
    x, y = np.meshgrid(xin, yin)
    print( "shape X", x.shape, "shape Y", y.shape, "shape Z" , z.shape)
    x=np.ma.array(x,mask=z.mask)
    y=np.ma.array(y,mask=z.mask)
    vmask=~np.ma.getmaskarray(z)     # z.mask
    valid=z[vmask].size
    #coeffs=np.ones(5)       # 1, x, y, x^2, y^2
    powers=((0,0),(1,0),(0,1),(2,0),(0,2))
    print ("valid data " , valid, "  num coeffs ", len(powers))

    a = np.ma.zeros((valid, len(powers)))

    for index,(i,j) in enumerate(powers):       #enumerate powers as columns
        aux= x**i * y**j
        a[:,index]=aux[vmask]                   #fill columns
    #print(a.shape); print(a)
    sol= np.linalg.lstsq(a, z[vmask], rcond=None)
    coeffs=sol[0]
    # compute residual on all points
    residues=residues=z.copy()
    for index,(i,j) in enumerate(powers):
        residues-=coeffs[index] * x**i * y**j

    return (coeffs, residues) # return  the coeffs of the solution and the residual matrix


# Replacement of periodogram with NFFT skipping NaN values


def holey_periodogram(data, fs=1.0, window='boxcar', FFTlength=None, detrend='constant', return_onesided=True, scaling='density', axis=-1):
    
    windata=data*scipy.signal.get_window(window, data.shape[1])
    xbase=np.linspace(-0.5, 0.5, num= data.shape[1], endpoint=False)
    # print(xbase)
    if FFTlength:
        N=2*(FFTlength//2)
    else :
        N= 2*(data.shape[1]//2)
    N2=N//2
    F = np.ma.empty(dtype=np.cdouble, shape=(data.shape[0], N) )
    for row in range(data.shape[0]):
        segment=np.ma.array(scipy.signal.detrend(windata[row, :], type=detrend), mask=windata[row, :].mask)
        num_invalid=segment[segment.mask].size
        if num_invalid :
            print (row, ":" , num_invalid, "invalid data points")
            if num_invalid > 0.2 * data.shape[1] :
                print( "   too many datapoints, line will be skipped from computation")
                F[row,:]=np.ma.masked_all(shape=(N,))
                continue
        x=xbase[~ segment.mask]
        y=segment[~ segment.mask]
        # F[row,:]=np.ma.zeros(shape=(N,))
        F[row,:]=nfft.nfft_adjoint(x,y,N, sigma=20, tol=1E-8, m=None, kernel='bspline')
        # if num_invalid :
            # print (x.shape,y.shape)

    if scaling == 'density' :
        if return_onesided :
            psd=np.square(np.abs(F[:,N2:]))*2/(N*fs)
            psd[:,0]=np.square(np.abs(F[:,N2]))/(N*fs)
            return ( np.linspace(0, 0.5*fs, num= N2, endpoint=False), psd)
        else :
            return ( np.linspace((-0.5+1/N)*fs, 0.5*fs, num= N2, endpoint=True),   np.square(np.abs(F))/(N*fs))
    elif scaling == 'spectrum' :
        #Not shure this is what scipy.signal calls spectrum
        if return_onesided :
            return ( np.linspace(0, 0.5*fs, num= N2, endpoint=False),   np.square(np.abs(F[:,N2:])/N))
        else :
            return ( np.linspace((-0.5+1/N)*fs, 0.5*fs, num= N2, endpoint=True),   np.square(np.abs(F)/N))
'''
def spline_interpolateA(data, window=('tukey', 0.2) ):
    windata=data*scipy.signal.get_window(window, data.shape[1])
    xbase=np.arange(0., data.shape[1],dtype=float)
    outrow=0
    invalids=0
    outdata=np.ma.empty(dtype=float, shape=data.shape)
    for inrow in range(data.shape[0]):
        segment=np.ma.array(windata[inrow, :], mask=windata[inrow, :].mask)
        if segment.mask[0]:
            segment[0]=0
            segment.mask[0]=False
        if segment.mask[-1]:
            segment[-1]=0
            segment.mask[-1]=False
                
        num_invalid=segment[segment.mask].size
        if num_invalid :
            print (inrow, ":" , num_invalid, "invalid data points")
            if num_invalid > 0.2 * data.shape[1] :
                print( "   too many datapoints, line will be skipped from computation")
                invalids+=data.shape[1]
                continue
                
            invalids+=num_invalid
            
            x=xbase[~ segment.mask]
            y=segment[~ segment.mask]
            # defining 1st derivative null at ends since Tuckey window was applied (periodic is not working)
            spline3=scipy.interpolate.make_interp_spline(x, y, bc_type=([(1, 0.0)], [(1, 0.0)]))
            outdata[outrow,:]=spline3(xbase, extrapolate='periodic')
        else:
            outdata[outrow,:]=windata[inrow,:]
        outrow+=1
    np.ma.resize(outdata, (outrow,))
    print ("percent of invalid data=", 100*invalids/(data.shape[0]*data.shape[1]) )
    print("output shape", outdata.shape)
    return outdata
    
'''

def spline_interpolate(data, rowwise=True, window=('tukey', 0.2) ):
    if rowwise:
        segment_size=data.shape[1]
        segment_num=data.shape[0]
        windata=data*scipy.signal.get_window(window, segment_size)
    else: 
        segment_size=data.shape[0]
        segment_num=data.shape[1]
        windata=data*scipy.signal.get_window(window, segment_size).reshape(segment_size,1)

    xbase=np.arange(0., segment_size,dtype=float)
    index_out=0
    invalids=0
    outdata=np.ma.empty(dtype=float, shape=data.shape)
    for index_in in range(segment_num):
        
        segment=np.ma.array(windata[index_in, :], mask=windata[index_in, :].mask) if rowwise else \
                np.ma.array(windata[:,index_in], mask=windata[:,index_in].mask)
        if segment.mask[0]:
            segment[0]=0
            segment.mask[0]=False
        if segment.mask[-1]:
            segment[-1]=0
            segment.mask[-1]=False
                
        num_invalid=segment[segment.mask].size
        if num_invalid :
            print (index_in, ":" , num_invalid, "invalid data points")
            if num_invalid > 0.2 * segment_size :
                print( "   too many datapoints, row/col will be skipped from computation")
                invalids+=segment_size
                continue
                
            invalids+=num_invalid
            
            x=xbase[~ segment.mask]
            y=segment[~ segment.mask]
            # defining 1st derivative null at ends since Tuckey window was applied (periodic is not working)
            spline3=scipy.interpolate.make_interp_spline(x, y, bc_type=([(1, 0.0)], [(1, 0.0)]))
            if rowwise:
                outdata[index_out,:]=spline3(xbase, extrapolate='periodic')
            else:
                outdata[:,index_out]=spline3(xbase, extrapolate='periodic')
        else:
            if  rowwise:
                outdata[index_out,:]=windata[index_in,:]
            else:
                outdata[:,index_out]=windata[:,index_in]
        index_out+=1
    if  rowwise:
        np.ma.resize(outdata, (index_out,))
    else:
        np.ma.resize(outdata, (data.shape[0],index_out))
    print ("percent of invalid data=", 100*invalids/(data.shape[0]*data.shape[1]) )
    print("output shape", outdata.shape)
    return outdata
    
def makePowerList(nx,ny, N):
    """! Create a power list suitable for use with fit2D_poly where
    @param nx is the maximum degree in X
    @param ny is the maximum degree in Y
    @param N is the maximum  total degree in  X and Y
    """
    plist=[]
    for npow in range(N+1):
        for ix in range(min(npow,nx),-1,-1):
            iy=npow-ix
            if iy>ny :
                break
            plist.append((ix,iy))
    return plist

def fit_conic_cylinder(xin,yin,z, guess=None,variability=(0,1,1,1,0,1)):
    """! fit the Z function tabulated with grid values xin and yin, by a conic cylinder defined by a tuplet ( 1/p,1/q, theta, x0,y0, alpha)
    @param xin the value of X sampling of the array Z
    @param yin the value of Y sampling of the array Z
    @para z the arrays of values to fit with a conic cylinder
    @param guess the tuplet of 6 initial values ( 1/p,1/q, theta, x0,y0, alpha) where
        p is a the signed distance of entrance focus to apex point, q is the exist distance of exit focus to apex, 
        theta is the grazing angle of focal segments to the tangent at apex
        x0, y0 is the apex location, alpha (radians) is the orientation of the conic generatrix line in the x,y plane
        two parameters xtilt and ytilt are alway added with itial guess = 0. The must not be specified
    @param variability a tuplet of 6 logic values  0 or 1 indicating if the corresponding parameter is fix or allowed o be adjusted (0 fix, 1 adjusted)
    @return the tuplet of 9 parameters completed with tilts ( 1/p,1/q, theta, x0,y0, alpha, z0, xtilt, ytilt)
    """
    from conic import conic
    import scipy.optimize
    if guess==None:
        print('No initial guess values given')
        return
    if len(guess) !=6:
        print('guess tuplets should contain 6 initial values')
    if len(variability) !=6 :
        print('variability tuplet should contain 6 values')
        return   
    #parameters=dict(pinv=guess[0], qinv=guess[1], theta=guess[2], x0= guess[3], y0=guess[4]
    param_name=('pinv','qinv','theta', 'x0', 'y0', 'alpha', 'z0', 'xtilt', 'ytilt')
    guess+=(0,0,0)
    variability+=(1,1,1)
    vmask=~z.mask
    x, y = np.meshgrid(xin, yin)
    paramcount=0
    for index, v in enumerate(variability):
        if v!=0:
            paramcount+=1
    def residuals(optvars):
        param=()
        i=0
        for index, v in enumerate(variability):
            if v==0:
                param+=(guess[index],)
            else:
                param+=(optvars[i],)
                i+=1
        xm=(x-param[3])[vmask]
        ym=(y-param[4])[vmask]
        ksi=xm*cos(param[5])+ym*sin(param[5])
        # vectorize the intercept function for applying it to ksi
        crv=conic(param[0], param[1],param[2])
        zdif=z[vmask]-crv.vector_intercept(ksi)-param[6]-xm*param[7]-ym*param[8]    # (param[8]+ym*param[9])
        return zdif
    # now we need to call the regression on optvars that minimize the norm of the returned difference
    # fill the initial param list
    print('num optim parameters',paramcount)
    initparam=np.zeros((paramcount,))
    i=0
    for index, v in enumerate(variability):
        if v!=0:
            initparam[i]=guess[index]
            i+=1
    print('initial parameters',initparam)
    result=scipy.optimize.least_squares(residuals, initparam,method='lm')
    if result.status == -1:
        endmsg="Badinput parameters"
    elif result.status == 0:
        endmsg="Convergence not reached"
    elif result.status == 1:
        endmsg="Gradient convergence stalling"
    elif result.status == 2:
        endmsg="Cost function minimized"
    elif result.status == 3:
        endmsg="Parameter convergence stalling"
    elif result.status == 4:
        endmsg="cost function and pameter convergence"
    
    print('optimisation status', result.status, endmsg)
    
    print('parameters', tuple(result.x))
    print('cost value', result.cost)
    ze=np.ma.array(np.zeros(z.shape), mask=z.mask)
    ze[vmask]=result.fun
    parameters=()
    i=0
    for index, v in enumerate(variability):
        if v==0:
            parameters+=(guess[index],)
        else:
            parameters+=(result.x[i],)
            i+=1
        print(param_name[index], parameters[index])
    
    return ze, parameters