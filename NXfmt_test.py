# -*- coding: utf-8 -*-

'''
This file is part of the Superflat project

Test program of Metrology NeXus format
'''

__author__  = "François Polack"
__copyright__ = "2023, Superflat-PCP project"
__email__ = "superflat-pcp@synchrotron-soleil.fr"
__version__ = "0.1.0"

import sys
sys.path.append('D:\projets\ProjetsPython\Superflat')
#import numpy as np
import heightmap
from pathlib import Path

def fiducial_list(fname):
    if fname=='Stitching_SESO_ALBA_ZOOMx1_7':
        flist=dict()
        flist['C1']=dict(unit='pixel', mark='cross', coordinates=(253,605))
        flist['C2']=dict(unit='pixel', mark='cross', coordinates=(1514.5,606))
        flist['C3']=dict(unit='pixel', mark='cross', coordinates=(1515.5,22.5))
        flist['C4']=dict(unit='pixel', mark='cross', coordinates=(884,21))
        flist['C5']=dict(unit='pixel', mark='cross', coordinates=(254,21.5))
        flist['P1']=dict(unit='pixel', mark='pit', coordinates=(83,310))
        flist['P2']=dict(unit='pixel', mark='pit', coordinates=(1637,312))
        flist['Pc1']=dict(unit='pixel', mark='circle', coordinates=(108,312))
        flist['Pc2']=dict(unit='pixel', mark='circle', coordinates=(1662,314))
        return flist
    else :
        return None

if __name__ == "__main__":
    hmp = heightmap.load('')          # create a new HeightMap object
    if hmp== None :
        print("No file loaded")
        exit(-1)
    fname=Path(hmp.filepath).stem
    outfile='nexus/NXmetro_format_example2'
    hmp.fiducial_list=fiducial_list(fname)
    hmp.reference_point=dict(name='C1', coordinates=(0.005, -0.015 ), unit='meter')
    hmp.sample_drawing="fiducials.svg"
    hmp.nexus_save(outfile)

