# -*- coding: utf-8 -*-

## @file conic.py
# This file provides a class for a conic which is tangent to the X axis at the origin and defined by the focal distance and angle (p q, theta)
#  The file defines the conic class, which can represent either an ellipse if (p*q) < 0, either an hyperbola if (p*q) > 0 or a parabola if 1/p=0 or 1/q =0
#   @author François Polack
#   @copyright 2023, Superflat-PCP project  <mailto:superflat-pcp@synchrotron-soleil.fr>
#   @version 0.1.0

## @mainpage Superflat Scripts toolkit


__author__  = "François Polack"
__copyright__ = "2023, Superflat-PCP project"
__email__ = "superflat-pcp@synchrotron-soleil.fr"
__version__ = "0.1.0"

import numpy as np
from math import sqrt, sin, cos, tan , atan, nan
import warnings


class conic(object):

    def __init__(self,pinv ,qinv, theta):
        """! Create a conic object the type of conic depends on the sign of the product pinv * qinv ellipse if <0, parabola if =0 , hyperbola if >0
        @param pinv  The inverse distance of entrance focus (x(f1) has the sign of pinv)
        @param qinv  The inverse distance of exit focus (x(f2) has the sign of qinv)
        @param theta the angle of the focal segments with the x axis (grazing angle) in radians
        """
        self.p=-1./pinv
        self.q=1./qinv
        self.theta=theta
        self.sintheta=sin(theta)
        self.tanphi=(pinv+qinv)/(pinv-qinv ) * tan(theta)
        self.phi=atan(self.tanphi)
        self.tanphi2=self.tanphi*self.tanphi
        
        self.m11=qinv-pinv
        self.m00=-4.*qinv*pinv/self.m11*self.sintheta**2
        
        self.b2=2*self.sintheta/(cos(self.phi)**2)
        self.a=self.m00*self.tanphi2+self.m11
        self.b1=(self.m00-self.m11)*self.tanphi
        self.c=self.m00+self.m11*self.tanphi2
        
    def intercept(self, X0):
        b=self.b1*X0-self.b2
        delta= b*b - self.a*self.c*X0*X0
        try:
            y=(-b-sqrt(delta))/self.a
            print (sqrt(delta)/self.a, b/self.a)
        except ValueError:
            print("Intercept equation has no solution")
            y= nan
        return y
        
    # def vector_intercept(self,x):  
        # flag=0 
        # y=np.zeros(x.shape)
        # for i, X0 in enumerate(x):
            # b=self.b1*X0-self.b2
            # delta= b*b - self.a*self.c*X0*X0
            # try:
                # y[i]=(-b-sqrt(delta))/self.a
            # except ValueError:
                # y[i]=nan
                # flag+=1
        # if flag!=0:
            # print('error trace in conic.vector intercept:  no solution for ', flag,' values')
        # return y
        
    def vector_intercept(self,x):  
        b=self.b1*x-self.b2
        delta= b*b - self.a*self.c*x*x
        with warnings.catch_warnings(record=True) as w:
            y=(-b-np.sqrt(delta))/self.a  
            if len(w) > 0:
                if issubclass(w[-1].category, RuntimeWarning):
                    print('error trace in conic.vector intercept:  no solution for ', y[np.isnan(y)].shape[-1],' values')
                else:
                    print(w[-1].category, ':', w[-1].message)
        return y
            
            
    def test(self, x):
        a=158
        def fun(x):
            return a-2*x
        print(fun(x))