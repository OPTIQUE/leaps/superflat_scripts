# -*- coding: utf-8 -*-

## @file heightmap.py
# This file provides tools for analysing surface height data with statistical and PSD tools.
#
# The file defines the HeightMap class, which is the main storage element of surface height measurements.
#   @author François Polack, Uwe Flechsig
#   @copyright 2022, Superflat-PCP project  <mailto:superflat-pcp@synchrotron-soleil.fr>
#   @version 0.1.0

## @mainpage Superflat Scripts toolkit
#   This package offers a collection of scripts for processing Optical surface Metrology data.
#
#   It aims at providing standardized tools for evaluating the optical quality of a surface in conformity to the
#   specifications detailed in the SuperFlat-PCP Request for Tender
#
#   @section Heightmap
#   The file heightmap.py defines the HeightMap class which is an data container for evaluating height data recorded from interferometric measurements.
#
#   @section processing
#   The file processing.py defines functions which can be used to detrend data and calculate the PSD with the Tukey as prescribed in the Superflat-PCP Request for Tender
#

__author__  = "François Polack, Uwe Flechsig"
__copyright__ = "2022, Superflat-PCP project"
__email__ = "superflat-pcp@synchrotron-soleil.fr"
__version__ = "0.1.0"

import numpy as np
import scipy.signal
import math
import tkinter
from tkinter import filedialog
from pathlib import Path
import h5py as h5
import matplotlib.pyplot as plt
import matplotlib.figure as fig
import nexusformat.nexus.tree as nx
import ZygoDat
import ZygoXYZ
import ZygoDatx
from processing import fit2D_poly, fit_toroid, fit2D_order2, fit_toroid1, holey_periodogram, \
        spline_interpolate, makePowerList, fit_conic_cylinder
from activefigure import new_active_figure, Viewer2D, on_mouse_move
import warnings
import pickle
from pathlib import Path


class HeightMap(object):
    '''! @brief The HeightMap object-
        - it stores the height map  from an interferometric measurement file
        - it defines a rectangular ROI, on which slope errrors and PSDs, and statistics are computed.
        - It allows polynomal 2D fits and detrending even if invalid data are present in the selected area
        - It performs slope, PSD and stistics computation on the ROI
        - it offers functions for plotting the computed quantities

        Computation results are stored in class member numpy arrays  @see public attribute list

    '''

    ## the class  constructor
    #
    # All public variables of the class are defined in this function
    def __init__(self,*,file=None):

        self.filepath=''

        ## (float,float) The pixel dimension [x,y] (in m).
        self.pixel=[0,0]
        ## (int, int ) The offset (in pixels) of the height map top-left corner in absolute coordinate. (used for map plots coordinates).
        self.origin=[0,0]
        ## (int, int ) Offset of the ROI with respect to raw image origin (pixels)
        self.ROIorigin=[0,0]
        ## (int, int) Size of the ROI in pixels (this is the size of Z, ze ... arrayss)
        self.ROIsize=[0,0]
        ## an integer or a twoplet indicating the axis which must be reverted
        self.flipROI=None
        ## Number of invalid values in the ROI
        self.num_invalid=0
        ## Original fullsize  height data loaded from the file.
        self.rawZ=None
        ## scale of the fast varying direction (index=1).        
        self.rawX=None
        ## scale of the slow varying direction (index=0).        
        self.rawY=None
        ## the height z(x,y) inside the ROI area
        self.z = None
        ## the height error inside the ROI area.
        self.ze = None
        ## the fast varying direction (index=1).
        self.x = None
        ## the slow varying direction (index=0).
        self.y = None
        ##  slope dze/dx
        self.dzdx = None
        ##  slope dze/dy
        self.dzdy = None
        ## the fit parameter (rot,rx, pitch, ry, roll, Z0)  [unused]
        self.fitparams = None

        ## slope rms CPSD
        self.srms = None # cumulated tangential slope error
        self.srms_sag=None #cumulated sagittal slope error

        ## tangential slope PSD frequency array
        self.sf = None
        ## sagittal slope PSD frequency array
        self.sf_sag = None
        
        ## detrending method used
        self.detrend= None
        ## memorization of detrend curvatures & orientation
        self.angle=None
        self.Cx=None
        self.Cy=None
        ## fiducial list and sample drawing defined 25/09/2023
        self.fiducial_list=None
        self.reference_point=None
        self.sample_drawing=None
        self.curveparms=None
        #added 15/11/2023
        self.tanCSrms_line=None
        self.sagCSrms_line=None
        #added 20/03/2024
        self.hrms=None
        self.hrms_sag=None
        # added 03/04/2024 storage for 1D-PSD
        self.PSD_tan=None
        self.PSD_sag=None

    def read_Zygofile(self, filename='', origin='zero' ):
        """! Reads a Zygo datafile in .datx, .dat or xyz format, and loads it into the HeightMap object.
        @param filename  [optional] full path to the file. If not given or en empty string, a fileopen dialog will pop-up.
        @param origin [optional] either:
        - a tuple (x0,y0) of the address of the top left corner of the loaded image in pixels
        - 'file', the origin from top left corner of the camera will be used in this case
        - 'zero'  which is equivalent to giving (0,0) to the parameter
        @return True if the HeightMap was successfully loaded, False otherwise.

        this function load the phase data in numpy masked arrays `HeightMap.rawZ` and `HeightMap.z`.
        Invalid data are set to NaN, and then masked to be excluded from computation

        `HeightMap.rawZ` is kept unmodified throughout processing to avoid reloading the data from the file\n
        `HeightMap.z` will be re-dimensioned to the selected  ROI

        @b CAUTION  It seems that multiplying a `numpy.ma.array` by a scalar on the right is corrupting the mask,
        while multiplying on the left appears to give a correct result
        """
        if not Path(filename).is_file():
            tkinter.Tk().withdraw()
            filename=filedialog.askopenfilename(title="Open a Zygo data file", filetypes=(("Zygo dat","*.dat"), ("Zygo datx","*.datx"),
                ("Zygo XYZ", "*.xyz"), ("Zygo ascii","*.ascii"), ("Lei matlab","*.mat") ))

        if Path(filename).suffix==".dat":
            datfile=ZygoDat.DatFile()
            datfile.info()
        elif Path(filename).suffix==".xyz" or Path(filename).suffix==".ascii":
            datfile=ZygoXYZ.XYZfile()
        elif Path(filename).suffix==".datx":
            datfile=ZygoDatx.DatxFile()
        else:
            print("Unknown file type:", Path(filename).suffix)
            return False
        if datfile.openFile(filename):      #if false invalid file message is printed
            self.filepath=filename
            self.rawZ=self.z=np.ma.masked_invalid( datfile.getHeight())
            if self.z.size==0:
                return False

            fileorigin=datfile.getOrigin()

            pixel=datfile.getPixel() #datx return a tuple , not dat and xyz
            if type(pixel)==list :
                self.pixel=pixel
            else:
                self.pixel[1]=self.pixel[0]=datfile.getPixel()

            if self.pixel[0]==0 or self.pixel[1]==0 : # this may happen in  processed data
                print("The pixel size is not documented in this file.")
                if self.pixel[0]==0:
                    instr=input("Enter X size in mm or empty input to cancel loading >")
                    if instr=="":
                        return False
                    else:
                        self.pixel[0]=float(instr)*1e-3
                if self.pixel[1]==0:
                    instr=input("Enter Y size in mm or empty input to cancel loading >")
                    if instr=="":
                        return False
                    else:
                        self.pixel[1]=float(instr)*1e-3

        else:
            return False

        self.ze=None
        print("pixel size=",1e3*self.pixel[0], 'x', 1e3*self.pixel[1], " mm^2")
        print("file origin of phase image: ", self.origin)
        if origin== 'zero':
            self.origin=[0,0]
        elif origin=='file':
            self.origin=fileorigin
        else:
            try:
                self.origin=origin
            except:
                print(origin, " is an invalid origin value, (0,0) will be used")
                self.origin=[0,0]
        self.ROIorigin=(0,0)

        self.ROIsize=[self.rawZ.shape[1]-1,self.rawZ.shape[0]-1]

        size=(self.pixel[0]*(self.rawZ.shape[1]-1),self.pixel[1]*(self.rawZ.shape[0]-1))
        org=(self.pixel[0]*self.origin[0], self.pixel[1]*self.origin[1])
        # org corrigé le 25/09/23 - FP
        #org=(self.pixel[0]*self.origin[0], self.pixel[1]*self.origin[1])

        self.x=self.rawX=np.linspace(org[0]+size[0], org[0], num=self.rawZ.shape[1], endpoint=True)
        self.y=self.rawY=np.linspace(org[1]+size[1], org[1], num=self.rawZ.shape[0], endpoint=True)
        self.num_invalid=self.rawZ[self.rawZ.mask].size
        print("Map size  {:d} x {:d} pixels^2   dimension {:.1f} x {:.1f} mm^2  {:d} invalid data points".
                format(self.rawZ.shape[1], self.rawZ.shape[0], 1e3*size[0], 1e3*size[1], self.num_invalid))
        return True
#end read_Zygofile

    def read_matfile(self, filename='' ):
        if not Path(filename).is_file():
            tkinter.Tk().withdraw()
            filename=filedialog.askopenfilename(title="Open matlab data file", filetypes=( ("Matlab file","*.mat"), ))
        if Path(filename).suffix==".mat":
            import scipy.io as sio
            print(sio.whosmat(filename))
            data=sio.loadmat(filename)
            if 'z2d_res' in data.keys():
                zmat='z2d_res'
            elif 'z2d' in data.keys():
                zmat='z2d'
            else:
                print('z data not found')
                return False
            self.filepath=filename
            self.x=self.rawX=np.copy(data['x2d'][0,:])
            self.y=self.rawY=np.copy(data['y2d'][:,0])
            # print("x\n",self.x.shape, data['x2d'].shape)
            print('x\n',self.x)
            print('y\n',self.y)
            self.origin=[self.x[0], self.y[0]]
            self.pixel=[abs(self.x[1]-self.x[0]), abs(self.y[1]-self.y[0])]
            self.ze=self.z=self.rawZ=np.ma.masked_invalid( data[zmat])
        else:
            return False
        print("pixel size=",1e3*self.pixel[0], 'x', 1e3*self.pixel[1], " mm^2")
        print("file origin of phase image: ", self.origin)
        self.ROIorigin=(0,0)
        self.ROIsize=[self.z.shape[1],self.z.shape[0]]
        self.num_invalid=self.z[self.z.mask].size
        size=(self.pixel[0]*self.rawZ.shape[1],self.pixel[1]*self.rawZ.shape[0])
        print("Map size  {:d} x {:d} pixels^2   dimension {:.1f} x {:.1f} mm^2  {:d} invalid data points".
        format(self.rawZ.shape[1], self.rawZ.shape[0], 1e3*size[0], 1e3*size[1], self.num_invalid))
        return True
        
    def set_scale(self, offset=(0,0)):
        """! 
            ------- THIS function is no longer used -------------
                    -------------------------------
        defines the scale of coordinate axis of the active `HeightMap` arrays `HeightMap.z` and `HeightMap.ze
        @param offset [optional]  the display origin of the active arrays

        HeightMap.set_scale redefines the `HeightMap.x` and `HeightMap.y` array which are internally used for displaying an height map.\n
        This function is  called internally after loading new data or changing the ROI.

        User should not need to call this function unless a new origin of the display coordinates is wanted
        """
        # old method changed to use  rawX,Y scale
        # size=(self.pixel[0]*self.z.shape[1],self.pixel[1]*self.z.shape[0])
        # # org corrigé le 25/09/23 - FP
        # org=(self.pixel[0]*(offset[0]+self.origin[0]), self.pixel[1]*(offset[1]+self.origin[1]))

        # self.x=np.linspace(org[0],org[0]+size[0],num=self.z.shape[1], endpoint=False)
        # self.y=np.linspace(org[1],org[1]+size[1],num=self.z.shape[0], endpoint=False)
        # self.num_invalid=self.z[self.z.mask].size
        # print("Map size  {:d} x {:d} pixels^2   dimension {:.1f} x {:.1f} mm^2  {:d} invalid data points".
                # format(self.z.shape[1], self.z.shape[0], 1e3*size[0], 1e3*size[1], self.num_invalid))
        self.num_invalid=self.z[self.z.mask].size
        self.x=self.rawX[offset[0]:self.z.shape[1]+offset[0]]
        self.y=self.rawY[offset[1]:self.z.shape[0]+offset[1]]
        size=(self.pixel[0]*self.z.shape[1],self.pixel[1]*self.z.shape[0])
        print("Map size  {:d} x {:d} pixels^2   dimension {:.1f} x {:.1f} mm^2  {:d} invalid data points".
                format(self.z.shape[1], self.z.shape[0], 1e3*size[0], 1e3*size[1], self.num_invalid))
#end set_scale


    def find_best_toroid(self):
        """! Find the best second degree 2D polynomial fitting the `HeightMap.z` data on the ROI
            @return The tuple (**parameters**, **residuals**) where \n
                **parameters** is the tuple  (**angle**, **rx**, **pitch**, **ry**, **roll**, **Z0**) with:
                    - **angle** being the angle of rotation of the axes of the best fitting toroid with respect to current ccordinates (in radians)
                    - **rx** the radius of curvature in the rotated x direction @f$(in m^-1)@f$
                    - **pitch** the tilt angle at point (0,0) in the rotated x direction (in radians)
                    - **ry** the radius of curvature in the rotated Y direction @f$(in m^-1)@f$
                    - **roll** the tilt angle at point (0,0) in the rotated Y direction (in radians)
                    - **z0**  the piston displacement (in m) \n
                **residuals**  is a `numpy.ma.array` of the difference between the input data `HeightMap.z` with the best fitting polynomial

            HeightMap.find_best_toroid  calls internally the  processing.fit2D_order2 function
            It prints the fitting parameters on standard output and return a tuple containing the parameter list and  the fit residuals.
                   """
        ((Z0, pitch, roll, x2, xy, y2), residuals) = fit2D_order2(self.x,self.y,self.z)
        """
        print("Raw fit coeffs: Z0 = {:.3e}m, pitch = {:.4e}rad,  roll = {:.4e}rad, x2=  {:.3e}m-1, xy=  {:.3e}m-1, y2=  {:.3e}m-1".
            format(Z0, pitch, roll, x2, xy, y2))
        """
        d=x2-y2
        angle=math.atan(xy/d)
        d/=math.cos(angle)
        angle=0.5*(angle)
        # here Cx and Cy are not the curvatures but the 2nd degree coefficients of polynomial expansion when ratoated
        Cx=0.5*(x2+y2+d)
        Cy=0.5*(x2+y2-d)

        try:
            rx = 0.5/ Cx
        except ZeroDivisionError:
            rx=math.inf
        try:
            ry = 0.5/ Cy
        except ZeroDivisionError:
            ry=math.inf
        # print("debug: fit params:" , Z0, roll, pitch, x2, xy, y2)
        print("angle = {:.4e}rad, rx = {:.3e}m, pitch = {:.4e}rad, ry = {:.3e}m, roll = {:.4e}rad, Z0 = {:.3e}m".
              format(angle, rx, pitch, ry, roll, Z0))
        #verification  antidiagonal should be almost 0
        # print("verification")
        # M=np.array([[x2,0.5*xy],[0.5*xy, y2]])
        # R= np.array([[math.cos(angle), -math.sin(angle)],[math.sin(angle), math.cos(angle)]])

        # M=R.T@M@R
        # print(M)
        # print("R'x=",0.5/M[0,0], "R'y=", 0.5/M[1,1])
        return (angle, rx, pitch, ry, roll, Z0)
# end find_best_toroid


    def remove_toroid(self,rot=0):
        """! Detrend the height data by removing a best fit toroid, optionnally of given orientation
        @param rot [optional] the angle of toroid axis with the coordinate axis [in rad]

        The remove_toroid function computes the best toroidal fit for the given orientation by calling  processing.fit_toroid
        and assign the residuals of the fit to the height error array (HeightMap.ze)
        """
        print("\nRemove best toroid with axis orientation {:.3f}mrad=".format(1e3*rot))
        (fitcoeffs, self.ze) = fit_toroid(self.x,self.y,self.z, rot)
        (Z0, pitch, roll, x2, y2)=fitcoeffs
        self.angle=rot
        # these are the removed curvatures = 1/R
        self.Cx=2*x2
        self.Cy=2*y2
        
        try:
            ry = 0.5/ y2
        except ZeroDivisionError:
            ry=math.inf
        try:
            rx = 0.5/ x2
        except ZeroDivisionError:
            rx=math.inf
        self.fitparams=(rot,rx, pitch, ry, roll, Z0)
        print("debug: fit params:" , fitcoeffs)
        print("rx = {:.3e}m, pitch = {:.4e}rad, ry = {:.3e}m, roll = {:.4e}rad, Z0 = {:.3e}m".
              format(rx, pitch, ry, roll, Z0))
        self.detrend='tore'
#end remove_toroid
    def detrend_conic(self, params, variability=(0,1,1,1,0,1)):
        """! Detrend the height data by removing a best fit conic cylinder, 
        @param params a tuplet of six  1st guess values 1/p, 1/q, theta, x0, y0, alpha
        @variability a tuplet of six values 0 or 1, indicating if the corresponding parameter is included in the fit
        
        The function assign the residuals of the fit to the height error array (HeightMap.ze)
        """
        print(params)
        print(variability)
        self.ze, self.fitparams=fit_conic_cylinder(self.x,self.y,self.z, guess=params,variability=variability)
        self.detrend='conic'
        
    def check_conic_guess(self,params):
        from conic import conic
        print(params)
        vmask=~self.z.mask
        x, y = np.meshgrid(self.x-params[3], self.y-params[4])
        
        # print('X\n',x)
        # print('Y\n',y)

        xm=x[vmask]
        ym=y[vmask]
        ksi=xm*cos(params[5])+ym*sin(params[5])
        crv=conic(params[0], params[1],params[2])
        if self.ze==None:
            self.ze=np.zeros(self.z.shape)
        # self.ze[vmask]=crv.vector_intercept(ksi)
        self.ze[vmask]=self.z[vmask]-crv.vector_intercept(ksi)
        
    def detrend_poly2D(self,powers, rot=0, origin=(0,0)):
        """! Detrend the height data by removing a best fit polynomial in X and Y  
        @param powers the list of X and Y powers of the polynomial this list can be created by processing::makePowerList
        @param rot an optional roration angle if the polynomial function in rad
        the function will print the fit coefficients and return the fit residual in self.ze
        """
        (coefficients, self.ze)=fit2D_poly(self.x-origin[0], self.y-origin[1], self.z, powers, rot)
        print( "coefficients")
        for index,(i,j) in enumerate(powers):
            print("x^",i,"y^",j," x  ",coefficients[index])
        self.detrend='poly'

    def compute_x_slope(self) :
        """!  @brief Calculate on the ROI, the slope in the x direction, from the height data.
        The HeightMap.compute_x_slope function assign the computed slopes to the HeightMap.dzdx array

        Computation is done with np.gradient:\n
        The gradient is computed using second order accurate central differences in the interior points and
        either first or second order accurate one-sides (forward or backwards) differences at the boundaries. \n
        The returned gradient hence has the same shape as the input array.
        """
        if self.num_invalid !=0:
            print("\nWARNING --  ROI contains invalid data  -- WARNING\n          some slope values might be inaccurate")

        self.dzdx = np.ma.masked_invalid(np.gradient(self.ze, self.pixel[0], axis=-1 ) )  # derivates with respect of faster axis and mask for safety
#end compute_x_slope


    def compute_y_slope(self) :
        """!  @brief Calculate on the ROI, the slope in the y direction, from the height data.
        The HeightMap.compute_x_slope function assign the computed slopes to the HeightMap.dzdx array

        Computation is done with np.gradient:\n
        The gradient is computed using second order accurate central differences in the interior points and
        either first or second order accurate one-sides (forward or backwards) differences at the boundaries. \n
        The returned gradient hence has the same shape as the input array.
        """
        if self.num_invalid !=0:
            print("\nWARNING --  ROI contains invalid data  -- WARNING\n          some slope values might be inaccurate")

        self.dzdy = np.ma.masked_invalid(np.gradient(self.ze, self.pixel[0], axis=0 ) )  # derivates with respect of slower axis and mask for safety
#end compute_y_slope


    def compute_CPSD_old(self,  method='scipy'):
        """! @brief Compute PSDs and CPSDs of the data and store the cumulated rms slope in member variable HeightMap.srms
        
            The method can be 'scipy' meaning PSD computed with scipy.signal.periodogram, or 'NFFT' meaning that PSD will be
            computed by the function processing.holey_periodogram which uses NFFT and can handle NaN containing arrays
            
            The HeightMap.compute_CPSD function  compute first `the PSD of the height errors (HeightMap.ze)

            The slope PSD is deduced by multiplying by the squared  frequency  and cumulated.

            The square root of the cumulated slope PSD is assigned to HeightMap.srms and represents the cumulated rms slope over frequencies.\n
            The frequency sampling is stored in HeightMap.sf.
        """

        xf = 1.0 / self.pixel[0]  # sampling rate in x
        print ("xf=", xf)
        print("FT input type", self.ze.dtype)
        #compute from heights
        if method=='scipy':
            if self.num_invalid !=0:
                # print("\nWARNING --  ROI contains invalid values  -- WARNING\n            Results might be inaccurate")
                print("\n --  ROI contains invalid values  -- Using spline interpolation\n")
                windata=spline_interpolate(self.ze, window=('tukey', 0.2))
                (self.sf, h2psd) = scipy.signal.periodogram(windata, xf, window='boxcar', 
                                               return_onesided=True)
            else:
                (self.sf, h2psd) = scipy.signal.periodogram(self.ze, xf, window=('tukey', 0.2), 
                                               return_onesided=True)  # one sided psd with tukey (0.2) taper along last (fast varying) axis (=-1, default)
        elif method=='NFFT':
            (self.sf, h2psd) = holey_periodogram(self.ze, xf, window=('tukey', 0.2),
                                               return_onesided=True)
        else :
            print("invalid method. Cannot compute the PSD")
            return
        print("FToutput type", h2psd.dtype)
        
        h1psd = np.mean(h2psd, axis=0)       # average over slow varying axis
        s1psd = h1psd * (2 * np.pi * self.sf)**2  # height to slope in fourier space
        print("    TAILLE de la PSD ", h2psd.shape, "    taille input ", self.ze.shape)

        # compute from slopes
        # (sf, s2psd) = scipy.signal.periodogram(self.dzdx, wf, window=('tuckey',0.2),
                                               # return_onesided=True)  # one sided psd with tukey (0.2) taper
        # s1psd = np.mean(s2psd, axis=0)    # average over l
        # self.sf = sf

        # uncomment to reverse cumulation direction
        #  s1csd = np.cumsum(s1psd[::-1])[::-1] * (self.sf[1] - self.sf[0])
        s1csd = np.cumsum(s1psd) * (self.sf[1] - self.sf[0])
        self.srms = np.sqrt(s1csd)
        print("Total cumulated rms=", self.srms[self.srms.shape[0]-1]*1e6, "µrad")
#€nd compute_CPSD_old

    def compute_CPSD(self):
        print("tangential RMS slope error")
        (self.sf, self.srms, self.hrms,self.PSD_tan)=self.CRMSslope(axis=-1)
        print("sagittal RMS slope error")
        (self.sf_sag, self.srms_sag, self.hrms_sag, self.PSD_sag)=self.CRMSslope(axis=0)
        

    def CRMSslope(self, axis=-1):
        xf = 1.0 / self.pixel[1+axis]  # sampling rate in x if axis=-1
        #print ("xf=", xf)
        if self.num_invalid !=0:
            print("\n --  ROI contains invalid values  -- Using spline interpolation on rows/cols with missing values\n")
            windata=spline_interpolate(self.ze, rowwise=(axis==-1), window=('tukey', 0.2))
            (sf, h2psd) = scipy.signal.periodogram(windata, xf, window='boxcar', 
                           return_onesided=True, axis=axis)
        else:
            (sf, h2psd) = scipy.signal.periodogram(self.ze, xf, window=('tukey', 0.2), 
                           return_onesided=True, axis=axis)  # one sided psd with tukey (0.2) taper along axis (=-1, default)
        h1psd = np.mean(h2psd, axis=(1+axis) )      # average over opposite axis
        s1psd = h1psd * (2 * np.pi * sf)**2  # height to slope in fourier space
        print("    Size of the PSD ", h2psd.shape, "   size of input ", self.ze.shape)
        # uncomment to reverse cumulation direction
        #  s1csd = np.cumsum(s1psd[::-1])[::-1] * (sf[1] - sf[0])
        # ajout 20/03/24
        hcpsd=np.cumsum(h1psd) * (sf[1] - sf[0])
        hrms=np.sqrt(hcpsd)
        #
        s1csd = np.cumsum(s1psd) * (sf[1] - sf[0])
        srms = np.sqrt(s1csd)
        print("Total cumulated rms=", srms[srms.shape[0]-1]*1e6, "µrad")
        return (sf,srms, hrms,h1psd)

    def print_statistics(self, height = True, raw = False, percent=0):
        """! Print statistics excluding the specified percentage of outlier pixels.
        @param height (boolean) If **True**, prints height statistics, otherwise (**False**) prints slope statistics
        @param raw (boolean) If **False** print raw height (HeightMap.z) statistics, ortherwise (**False**) prints height error statistics (HeightMap.ze)
        @param percent the percent of pixels whose values are the most distant from average which are excluded from the statistics

        The statistics are printed on standard output
        """

        if height :
            if raw :
                tu=self.z
                title="height"
                unit="um"
                mult=1e6
            else:
                tu=self.ze
                title="height_error"
                unit="nm"
                mult=1e9
            tv=None
        else:
            tu=self.dzdx
            tv=self.dzdy
            title="slope X"
            title_y="slope Y"
            unit="urad"
            mult=1e6

        print("\nStatistics on full ROI size")
        if type(tu)!= type(None):
            print("{:s}: rms= {:.3f} {:s}, pv= {:.3f} {:s}".
                    format(title, np.ma.std(tu) * mult, unit, (np.ma.max(tu) - np.ma.min(tu)) * mult,unit))
        if type(tv)!= type(None):
            print("{:s}: rms= {:.3f} {:s}, pv= {:.3f} {:s}".
                    format(title_y, np.ma.std(tv) * mult, unit, (np.ma.max(tv) - np.ma.min(tv)) * mult,unit))
                    
        if percent != 0:
            bounds=np.array([percent/2, 100-percent/2])
            print("statistics on", 100-percent, "% of ROI surface")
            #percentile do no work with masked we need to force removing all masks
            if type(tu)!= type(None):
                vmask=~np.ma.getmaskarray(tu)
                vbounds=np.percentile(np.array(tu[vmask]),bounds)
                uu=np.ma.masked_outside(tu, vbounds[0],vbounds[1], copy=True)
                print("{:s}: rms= {:.3f} {:s}, pv= {:.3f} {:s}".
                    format(title, np.ma.std(uu) * mult, unit, (vbounds[1] - vbounds[0]) * mult, unit))
            if type(tv)!= type(None):
                vmask=~np.ma.getmaskarray(tv)
                vbounds=np.percentile(np.array(tv[vmask]),bounds)
                uu=np.ma.masked_outside(tv, vbounds[0],vbounds[1], copy=True)
                print("{:s}: rms= {:.3f} {:s}, pv= {:.3f} {:s}".
                    format(title_y, np.ma.std(uu) * mult, unit, (vbounds[1] - vbounds[0]) * mult, unit))                    

# end print_statistics
    def clip(self, origin, size):
        '''
            clip raw data to avoid full rows and columns of invalid data
        '''
        self.rawZ=self.rawZ[origin[1]:origin[1]+size[1], origin[0]:origin[0]+size[0]]
        # we need also to clip self.rawX and self.rawY
        self.rawX=self.rawX[origin[0]:origin[0]+size[0]]
        self.rawY=self.rawY[origin[1]:origin[1]+size[1]]

    def set_ROI(self, firstX, firstY, nwidth, nheight, flip=None):
        """! define a rectangular ROI and set the `HeightMap.z`array accordingly
        @param firstX origin of the ROI in x (pixels from `HeightMap.origin`)\n
        @param firstY origin of the ROI in y (pixels from `HeightMap.origin`)\n
        @param nwidth width of the ROI (pixels)
        @param nheight height of the ROI (pixels)
        @param flip int or tuple indicating the axes to flip
        """
        self.ROIorigin=[firstX, firstY]
        self.ROIsize=[nwidth, nheight]
        if flip!=None: 
            self.flipROI=flip
        self.z=self.rawZ[firstY:firstY+nheight, firstX:firstX+nwidth]
        print("\n ROI definition:")
        if self.flipROI==None: 
            self.z=self.rawZ[firstY:firstY+nheight, firstX:firstX+nwidth]
        else:
            self.z=np.flip(self.rawZ[firstY:firstY+nheight, firstX:firstX+nwidth],self.flipROI)
            print("ROI axes", self.flipROI, "are flipped over")
        self.ze=self.z
        print("original size ", (self.rawZ.shape[1],self.rawZ.shape[0]), "\nROI origin " , self.ROIorigin,\
            "    ROI size " , (self.z.shape[1],self.z.shape[0]))
        # self.set_scale(offset=(firstX,firstY))
        self.x=self.rawX[firstX:firstX+nwidth]
        self.y=self.rawY[firstY:firstY+nheight]
        
        self.num_invalid=self.ze[self.ze.mask].size
        print('ROI changed; new number of invalid points ', self.num_invalid)
 #end set_ROI


    def plot_select_ROI(self, raw=False, fit_degree=2 ) :
        """! Selects a ROI graphically  by drawing a rectangle on a figure of the height errors.
            @param raw if set to **False** the measured height stored in HeightMap.rawZ are displayed; otherwise, the residual from a second degree polynomial fit is displayed

            A figure of the heights or height errors on the full HeightMap.rawZ defined area is displayed.
            A rectangle can be selected with the mouse.\n
            A special button is available allowing to select the ROI and proceed with computations.
            If the figure is close without defining a ROI the previous ROI, if one was defined, or the full size, is kept
        """

        # we need to use a full size image and remove a 2nd degree trend for best view

        newfig=new_active_figure(self,raw=raw, percent=8, fit_degree=fit_degree)
        plt.show(block=False)                          # show plots on screen
        newfig.canvas.start_event_loop()
#end plot_select_ROI


    def plot2dheight(self, raw=False, contour=True, percent=5) :
        '''! Plot 2d height or height error in the ROI with/without contour lines
           @param raw (bool) If **False** raw height are plotted otherwise (default) height error
           @param contour (bool) If **True** contour lines are also plotted (default is **False**)
           @param percent the percent of pixels whose values are the most distant from average which are excluded of the display scale
        '''

        if raw :
            zlabel = 'z (µm)'
            zscale = 1e6
            title = 'raw height'
            uu = zscale*self.z
        else :
            zlabel = 'z (nm)'
            zscale = 1e9
            title = 'height error'
            uu = zscale*self.ze


        bounds=np.array([percent/2, 100-percent/2])
        #percentile do no work with masked arrays we need to force it
        vmask=~np.ma.getmaskarray(uu)     # uu.mask
        vbounds=np.percentile(np.array(uu[vmask]),bounds)

        # print( "map range: min/max = ", np.min(uu), np.max(uu), " percentile :" ,vbounds )

        (w, h)=fig.figaspect(uu)

        # warnings.simplefilter('ignore', category=UserWarning)
        plotfig=plt.figure(FigureClass=Viewer2D, figsize=(w, h), constrained_layout=True)
        # warnings.resetwarnings()

        # plotfig.gca().invert_yaxis()  # images are scanned in line order, from top left to bottom right
        # memorize the main axes for displaying info
        plotfig.selectaxes=plt.gca().axes
        plotfig.hmap=self
        plotfig.mvcid = plotfig.canvas.mpl_connect('motion_notify_event', on_mouse_move)
        plotfig.origin=self.ROIorigin
        plotfig.z= self.z if raw else self.ze
        plotfig.unit='µm' if raw else 'nm'
        plotfig.x=self.x*1e3
        plotfig.y=self.y*1e3
        if contour :
            plt.contour(plotfig.x, plotfig.y, uu, 15, linewidths= 0.5, colors= 'k')  # contour lines

        plt.pcolormesh(plotfig.x, plotfig.y, uu, cmap=plt.get_cmap('rainbow'),
                  vmin=vbounds[0], vmax=vbounds[1], shading='auto')
                 # vmin=np.min(uu), vmax=np.max(uu), shading='auto')

        cbar= plt.colorbar(aspect=15)
        cbar.set_label(zlabel) #, rotation=270)
        plt.xlabel('x (mm)')
        plt.ylabel('y (mm)')
        plt.title(title)

        return plotfig
# end plot2dheight


    def plot2dslope(self,direction='x', filter=None, percent=5, unit='µrad') :
        '''! 2D plot the slope gradient taken along the X or Y direction
            @param dierction 'x' or 'y' : the direction of derivation
            @param 'filter' tjhe cut off frequency of a gaussian filter applied for display (mm-1)
            @param percent the percent of pixels whose values are the most distant from average which are excluded of the display scale
        '''
        
        from scipy import ndimage as img

        plotfig=plt.figure(FigureClass=Viewer2D, figsize=fig.figaspect(self.dzdx), constrained_layout=True)


        # plotfig.gca().invert_yaxis()  # images are scanned in line order, from top left to bottom right
        
        plotfig.selectaxes=plt.gca().axes
        plotfig.hmap=self
        plotfig.mvcid = plotfig.canvas.mpl_connect('motion_notify_event', on_mouse_move)
        plotfig.origin=self.ROIorigin
         
        plotfig.unit=unit
        if unit =='mrad':
            scale=1e3
        elif unit=='µrad':
            scale=1e6
        elif unit=='nrad':
            scale=1e9

        bounds=np.array([percent/2, 100-percent/2])
        if direction=='x':
            vmask=~self.dzdx.mask
            if filter==None:
                slope_data=self.dzdx
            else:
                sigma=3e-4/filter/self.pixel[0]
                slope_data=img.gaussian_filter1d(self.dzdx, sigma, axis=0)
            title='Tangential slopes'
            zlabel = 'X slopes ('+unit+')'
        elif direction=='y':
            vmask=~self.dzdy.mask
            if filter==None:
                slope_data=self.dzdy
            else:
                sigma=3e-4/filter/self.pixel[0]
                slope_data=img.gaussian_filter1d(self.dzdy, sigma, axis=0)
            title='Sagittal slopes'
            zlabel = 'Y slopes ('+unit+')'
        else:
            print("undefined direction:",direction)
            return
        
        plotfig.z=slope_data       
        #we may have masked values percentile do no work with masked arrays we need to force it 
        
        vbounds=np.percentile(np.array(slope_data[vmask]),bounds)
        # print (slope_data.shape, self.x.shape, self.y.shape)

        #plt.pcolormesh(selftem.x * 1e3, self.y * 1e3, self.dzdx * 1e6, cmap=plt.get_cmap('rainbow'),
        #               vmin=np.min(self.dzdx) * 1e6, vmax=np.max(self.dzdx) * 1e6, shading='auto')
        plotfig.x=self.x*1e3
        plotfig.y=self.y*1e3
        plt.pcolormesh(plotfig.x, plotfig.y, slope_data*scale, cmap=plt.get_cmap('rainbow'),
                            vmin=vbounds[0]*scale, vmax=vbounds[1]*scale, shading='auto')

        cbar = plt.colorbar(aspect=15)
        
        cbar.set_label(zlabel) #, rotation=270)
        plt.xlabel('x (mm)')
        plt.ylabel('y (mm)')
        plt.title(title)
#end plot2dslope


    def plot_slope_rms(self, tangential=True, scale='log', comment='') :
        """! Display a graph of the cumulative rms slope error versus spatial frequency
            @param comment an optional comment which will be appended to the graph title

            The cumulative rms slope error is the square root of the slope CPSD
        """
        from crmsfig import create_crms_figure
        csfig=create_crms_figure(figsize=fig.figaspect(0.5))  # new plot window

        if tangential:
            plt.plot(self.sf*1e-3, self.srms*1e6)
            plt.title('Cumulated RMS tangential slopes {}'.format(comment))
        else:
            plt.plot(self.sf_sag*1e-3, self.srms_sag*1e6)
            plt.title('Cumulated RMS sagittal slopes {}'.format(comment))
        plt.grid(visible=True, which='major', axis='both')
        plt.xlabel(r'spatial frequency $(mm^{-1})$')
        plt.ylabel('rms slope (µrad)')
        if scale=='log':
            plt.loglog()
            csfig.canvas.mpl_connect('close_event', self.on_close_fig)
        else:
            plt.semilogx()  
# end plot_slope_rms

    def plot_height_rms(self, tangential=True, scale='log', comment='') :
        """! Display a graph of the cumulative rms height error versus spatial frequency
            @param comment an optional comment which will be appended to the graph title

            The cumulative rms height error is the square root of the height CPSD
        """
        from crmsfig import create_crms_figure
        csfig=create_crms_figure(figsize=fig.figaspect(0.5))  # new plot window

        if tangential:
            plt.plot(self.sf*1e-3, self.hrms*1e9)
            plt.title('Cumulated RMS tangential heights {}'.format(comment))
        else:
            plt.plot(self.sf_sag*1e-3, self.hrms_sag*1e9)
            plt.title('Cumulated RMS sagittal heights {}'.format(comment))
        plt.grid(visible=True, which='major', axis='both')
        plt.xlabel(r'spatial frequency $(mm^{-1})$')
        plt.ylabel('rms height (nm)')
        if scale=='log':
            plt.loglog()
            csfig.canvas.mpl_connect('close_event', self.on_close_fig)
        else:
            plt.semilogx()  
# end plot_height_rms

    def plot_PSD(self, tangential=True, scale='log', comment='') :
        """! Display a graph of the surface height PSD versus spatial frequency
            @param comment an optional comment which will be appended to the graph title
        """
        from crmsfig import create_crms_figure
        csfig=create_crms_figure(figsize=fig.figaspect(0.5))  # new plot window

        if tangential:
            plt.plot(self.sf*1e-3, self.PSD_tan*1e9)
            plt.title('Tangential PSD {}'.format(comment))
        else:
            plt.plot(self.sf_sag*1e-3, self.PSD_sag*1e9)
            plt.title('Sagittal PSD {}'.format(comment))
        plt.grid(visible=True, which='major', axis='both')
        plt.xlabel(r'spatial frequency $(mm^{-1})$')
        plt.ylabel('rms PSD $(mm^{3})$')
        if scale=='log':
            plt.loglog()
            csfig.canvas.mpl_connect('close_event', self.on_close_fig)
        else:
            plt.semilogx()  
# end plot_height_rms

    def on_close_fig(self,event):
        from crmsfig import Crms_fig
        if isinstance(event.canvas.figure, Crms_fig):
            if event.canvas.figure.gca().get_title()[:31]=='Cumulated RMS tangential slopes':
                self.tanCSrms_line=event.canvas.figure.vertices
            elif event.canvas.figure.gca().get_title()[:29]=='Cumulated RMS sagittal slopes':
                self.sagCSrms_line=event.canvas.figure.vertices

    def save(self, filepath=''):
        if not Path(filepath).parent.is_dir():
            tkinter.Tk().withdraw()
            filepath=filedialog.asksavefilename(title="open a hmap file", filetypes=(("superflat hmap","*.hmap"), ("all","*.*") ) )
        print("writing to", filepath)
        with open(filepath, 'wb') as f:
            pickle.dump(self, f)

    def get_curveparms(self):
        if self.curveparms ==None:
            linewidth=2
            linestyle='-'
            label=Path(self.filepath).stem
            color='black'
            print('Default curve parameters used')
            return (label,color,linewidth,linestyle)
        else:
            return self.curveparms
        
    def testplot(self, data, scale=1, percent=5):
        uu=data*scale
        
        bounds=np.array([percent/2, 100-percent/2])
        #percentile do no work with masked arrays we need to force it
        vmask=~np.ma.getmaskarray(uu)     # uu.mask
        vbounds=np.percentile(np.array(uu[vmask]),bounds)

        #print( "map range: min/max = ", np.min(uu), np.max(uu), " percentile :" ,vbounds )

        (w, h)=fig.figaspect(uu)

        warnings.simplefilter('ignore', category=UserWarning)
        plotfig=plt.figure(figsize=(w, h), constrained_layout=True)
        warnings.resetwarnings()

        # plotfig.gca().invert_yaxis()  # images are scanned in line order, from top left to bottom right

        plt.pcolormesh(self.x*1e-3, self.y*1e3, uu, cmap=plt.get_cmap('rainbow'),
                  vmin=vbounds[0], vmax=vbounds[1], shading='auto')
                 # vmin=np.min(uu), vmax=np.max(uu), shading='auto')

        cbar= plt.colorbar(aspect=15)
        cbar.set_label('windowed heights (nm)') #, rotation=270)
        plt.xlabel('x (mm)')
        plt.ylabel('y (mm)')
        plt.title("testplot")

    def nexus_save(self,filepath):
        from os import chdir
        fname=Path(filepath).with_name(Path(filepath).stem+'.nxs')
        print("creating file ", fname)
        # print("flipped axes", self.flipROI) WARNING  if ROI is flipped the reference frame of rawZ and z are different
        #define the absolute coordinates
        xpix=self.pixel[0]
        ypix=-self.pixel[1]
        # set the coordinate origin in pixel units
        if self.fiducial_list !=None and self.reference_point != None :
            refpoint=self.fiducial_list[self.reference_point['name']]
            # this is true if ref point is in meter and fiducials in pixels
            origin_x=refpoint['coordinates'][0]-self.reference_point['coordinates'][0]/xpix
            origin_y=refpoint['coordinates'][1]-self.reference_point['coordinates'][1]/ypix
            print ('origin =(', origin_x, ', ', origin_y, ')')
            print ('ROI origin =(', self.ROIorigin[0], ', ', self.ROIorigin[1], ')')
        else:
            origin_x=0
            origin_y=rawZ.shape[0]-1
        # Create raw_data 
        xraw=np.linspace(-origin_x*xpix,(self.rawZ.shape[1]-origin_x)*xpix,num=self.rawZ.shape[1], endpoint=False)
        yraw=np.linspace(-origin_y*ypix,(self.rawZ.shape[0]-origin_y)*ypix,num=self.rawZ.shape[0], endpoint=False)
        x=nx.NXfield(xraw, name='x', unit='meter')
        y=nx.NXfield(yraw, name='y', unit='meter')
        rawdata=nx.NXdata(nx.NXfield(self.rawZ, name='height', unit='meter'),[y,x],name='raw_data')
        #create detrended data group with height and slope fields
        height=nx.NXfield(self.ze, name='height', unit='meter')
        xview=np.linspace((self.ROIorigin[0]-origin_x)*xpix,(self.ROIorigin[0]+self.ROIsize[0]-origin_x)*xpix,num=self.z.shape[1], endpoint=False)
        yview=np.linspace((self.ROIorigin[1]-origin_y)*ypix,(self.ROIorigin[1]+self.ROIsize[1]-origin_y)*ypix,num=self.z.shape[0], endpoint=False)
        x=nx.NXfield(xview, name='x', unit='meter')
        y=nx.NXfield(yview, name='y', unit='meter')
        slope_x=nx.NXfield(self.dzdx, name='slope_x', unit='radian')
        slope_y=nx.NXfield(self.dzdy, name='slope_y', unit='radian')
        
        detrended_height=nx.NXdata(height,[y,x],  name='detrended_height')
        #create the process group with detrending parameters and ROI definition
        region=nx.NXgroup( name='ROI', nxclass='NXregion', start=self.ROIorigin, count=self.ROIsize)
        region.region_type=nx.NXattr('rectangular')
        process=nx.NXprocess(region)
        process.program=nx.NXattr('detrend')
        process.fit=nx.NXattr('toroid')
        process.radius_x=nx.NXfield(self.fitparams[1],unit='meter')
        process.radius_y=nx.NXfield(self.fitparams[3],unit='meter')
        process.rotation=nx.NXfield(self.fitparams[0],unit='radian')
        # the process.data group defining the process group will be a link. It cannot be set before the whole tree is created
        #attach the process group to the detrended data group
        detrended_height.process=process
        
        #create the sample and instrument group  
        sample=nx.NXsample()
        detector=nx.NXdetector(pixel_size_x=nx.NXfield(self.pixel[0], unit='meter'), pixel_size_y=nx.NXfield(self.pixel[1], unit='meter'))
        instrument= nx.NXinstrument(detector)
        
        #Create the measurement nx.NXentry  with the group which don't contain links
        entry=nx.NXentry(detrended_height,rawdata,sample,instrument, name='measurement1')
        entry.definition=nx.NXattr('NXmetrology')
        # add a the source data to the process group as alink
        process.data=nx.NXlink(rawdata)
        
        detrended_slopes=nx.NXdata(slope_x,[nx.NXlink(y),nx.NXlink(x)], slope_y=slope_y, name='detrended_slopes')
        detrended_slopes.auxiliary_signals=nx.NXattr('slope_y')
        #create the process group with detrending parameters and ROI definition
        process=nx.NXprocess()
        process.program=nx.NXattr('numerical_derivation')
        process.method=nx.NXattr('3 points')
        #attach the process group to the detrended data group
        detrended_slopes.process=process
        #Attach the slope group to the enntry
        entry.detrended_slopes=detrended_slopes
        #link the data to the process group
        process.data=nx.NXlink(detrended_height)
        
        # add the data group require by the NXmetrology definition 
        entry.data=nx.NXdata(nx.NXlink(height), [nx.NXlink(y), nx.NXlink(x)], slope_x=nx.NXlink(slope_x), slope_y=nx.NXlink(slope_y))
        entry.data.auxiliary_signals=nx.NXattr(['slope_x', 'slope_y'])
        entry.data.set_default()
        #add a drawing in the sample group if defined
        if self.sample_drawing!=None:
            add_drawing(sample,self.sample_drawing)
        #add a fiducial group to the sample group
        if self.fiducial_list != None:
            sample.fiducials=nx.NXgroup(nxclass='NXfiducials')
            for name in self.fiducial_list:
                fiducial=self.fiducial_list[name]
                coord=((fiducial['coordinates'][0]-origin_x)*xpix, (fiducial['coordinates'][1]-origin_y)*ypix)
                sample.fiducials.insert(nx.NXfield(coord, name=name, unit='meter'))
                0
                #print(name, ': ', fiducial['mark'], fiducial['coordinates'][0]*self.pixel[0]*1.e3, fiducial['coordinates'][1]*self.pixel[1]*1.e3,'mm')
        
        #print(sample['NXfiducials'])
        #put the measurement entry in the default path
        root=nx.NXroot(entry)
        entry.set_default()
        
        # erase the old file if any and write the NeXus tree to the file
        if Path(fname).exists():
            Path(fname).unlink()
        #avoid to save whole path in root.file_name
        cur=Path.cwd()
        chdir(fname.parent)
        root.save(str(fname.name))
        chdir(cur)
        
        # Example of data exploration
        # Locate the default entry
        main_entry=root[root.default]
        path=main_entry.data
        print('fields of ', path.nxpath)
        # Locate the group owning the fields of default datagroup
        for item in path:
            if path[item].nxtarget !=None:
                print(item,' is in ',root[path[item].nxtarget].nxgroup.nxpath)
            else:
                print(item,' is in ', path[item].nxgroup.nxpath)

def add_drawing(parent,filename):
    ext=Path(filename).suffix
    if ext=='.svg' or ext=='.dxf':
        with open(filename, 'r') as file:
            text=file.read()
            parent.drawing=nx.NXfield(text)
    elif ext=='.pdf':
        with open(filename,'rb') as file:
            filedata=file.read()
            parent.drawing=nx.NXfield(bytearray(filedata), dtype='uint8')
    else:
        print("Invalid drawing file extension: ", ext)
    parent.drawing.format=nx.NXattr(ext)

 
#  this module function returns a HeightMap object from a previously saved .hmap file
def load(filepath=''):
    if not Path(filepath).is_file():
        tkinter.Tk().withdraw()
        filepath=filedialog.askopenfilename(title="open a hmap file", filetypes=(("superflat hmap","*.hmap"), ("all","*.*") ) )
        if  filepath=='' :
            return None
    print("loading from", filepath)
    with open(filepath, 'rb') as f:
        return pickle.load(f)
